/**
 * Contient la config firebase
 */
const firebaseConfig = {
  apiKey: 'AIzaSyCgsyHxtkWaLkqeFdKSHJ0pTqGJuCin0Sw',
  authDomain: 'tbrochat.firebaseapp.com',
  databaseURL: 'https://tbrochat.firebaseio.com',
  projectId: 'tbrochat',
  storageBucket: 'gs://tbrochat-vcm',
};

export default firebaseConfig;

/**
 * Classe contenant les clés d'API nécessaires au projet
 */

const API_GOOGLE = 'AIzaSyCvieaHmMDxtXGOBKhmxp08He77Ud1Kr2M';
const API_VIROREACT = '646C9B34-8AB7-41E0-993F-F8E95333FD50';

export default { API_GOOGLE, API_VIROREACT };

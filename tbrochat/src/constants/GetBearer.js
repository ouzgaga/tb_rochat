/**
 * Permet d'obtenir les tokens pour utiliser les services Dialogflow et Google Cloud AutoML
 * Utilise une Cloud Function
 */
import keyfileAutoML from './keyfileAutoML.json';
import keyfileDialogflow from './keyfileDialogflow.json';

const getBearer = async (keyfile) => {
  const { client_email, private_key } = keyfile;

  try {
    const url = 'https://us-central1-tbrochat.cloudfunctions.net/getAccessToken';

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        email: client_email,
        private_key,
      }),

    });
    const responseJson = await response.json();
    return responseJson.token;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const getBearerAutoML = () => getBearer(keyfileAutoML);

const getBearerDialogflow = async () => getBearer(keyfileDialogflow);

export default { getBearerAutoML, getBearerDialogflow };

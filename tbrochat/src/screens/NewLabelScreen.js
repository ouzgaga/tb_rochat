/**
 * Permet d'ajouter un nouveau label à notre modèle
 */
import React, { useState, useContext } from 'react';
import {
  StyleSheet,
  Alert,
} from 'react-native';
import {
  Container, Item, Input, Button, Text, Content,
} from 'native-base';
import PropTypes from 'prop-types';

import Firebase from '../api/Firebase';
import { AppContext } from '../util/Store';
import Multilingue from '../util/Multilingue';
import HeaderMenu from '../navigation/HeaderMenu';

export default function NewLabelScreen({ navigation }) {
  const [newLabelValue, setNewLabelValue] = useState('');
  const appContext = useContext(AppContext);
  const { userLabels } = appContext;

  /**
   * La valeur du text field change
   * @param {*} value la nouvelle valeur du textfield
   */
  const onChangeText = (value) => {
    setNewLabelValue(value);
  };

  /**
   * Permet d'ajouter un nouveau label
   * Nécessite que le label passe les expressions regex et qu'il ne soit pas déjà existant
   */
  const addNewLabel = async () => {
    const re = /^[a-zA-Z0-9]\w*$/;
    if (re.test(newLabelValue)) {
      for (let i = 0; i < userLabels.length; i += 1) {
        if (userLabels[i].data.name === newLabelValue) {
          Alert.alert(
            Multilingue.translateText('error'),
            Multilingue.translateText('alreadyExistingLabel'),
            [
              {
                text: Multilingue.translateText('ok'),
                onPress: () => { },
              },
            ],
            { cancelable: false },
          );
          return;
        }
      }

      await Firebase.createLabel(newLabelValue);
      Alert.alert(
        '',
        Multilingue.translateText('labelAdded'),
        [
          {
            text: Multilingue.translateText('ok'),
            onPress: () => { setNewLabelValue(''); },
          },
        ],
        { cancelable: false },
      );
    } else {
      Alert.alert(
        Multilingue.translateText('errorMalformatedLabel'),
        Multilingue.translateText('malformatedLabelExplain'),
        [
          {
            text: Multilingue.translateText('ok'),
            onPress: () => { },
          },
        ],
        { cancelable: false },
      );
    }
  };

  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('newLabelMenu')} />

      <Content contentContainerStyle={styles.container}>
        <Item regular>
          <Input placeholder={Multilingue.translateText('labelName')} value={newLabelValue} onChangeText={onChangeText} />
        </Item>

        <Button style={styles.centeredButton} onPress={addNewLabel}>
          <Text>
            {Multilingue.translateText('addLabel')}
          </Text>
        </Button>

      </Content>
    </Container>
  );
}


NewLabelScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  centeredButton: {
    marginTop: 40,
    alignSelf: 'center',
  },
});

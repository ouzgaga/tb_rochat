/**
 * Menu pour entrainer un modèle
 */
import React, { useContext, useState, useEffect } from 'react';
import {
  Alert,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {
  Container, Content, Button, Text,
} from 'native-base';
import PropTypes from 'prop-types';

import { AppContext } from '../util/Store';

import Multilingue from '../util/Multilingue';
import Firebase from '../api/Firebase';
import GoogleAutoML from '../api/GoogleAutoML';
import HeaderMenu from '../navigation/HeaderMenu';

export default function TrainModelScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const {
    userLabels, userImages, tokenAutoml, datasetName, operationId, modelName, loading, setLoading,
  } = appContext;

  const [operationInProgress, setOperationInProgress] = useState(true);

  /**
   * Observe si une operation est terminée ou non
   * @param {*} operationIdToCheck l'id de l'opération
   */
  const checkIfOperationIsFinished = async (operationIdToCheck) => {
    if (operationIdToCheck) {
      const operationState = await GoogleAutoML.getOperationDetails(tokenAutoml, operationIdToCheck);

      if (operationState.done) {
        setOperationInProgress(false);
        if (operationState.metadata.partialFailures) {
          if (operationState.metadata.partialFailures.find(failure => failure.code === 7)) {
            Alert.alert(
              Multilingue.translateText('error'),
              Multilingue.translateText('errorDuplicateFiles'),
              [
                {
                  text: Multilingue.translateText('ok'),
                  onPress: () => { },
                },
              ],
              { cancelable: false },
            );
          }
        }

        if (operationState.metadata.createModelDetails) {
          Firebase.setAutoMLModel(operationState.response.name);
        }
      } else {
        setOperationInProgress(true);
        setTimeout(() => { checkIfOperationIsFinished(operationIdToCheck); }, 3000);
      }
    } else {
      setOperationInProgress(false);
    }
  };

  const firstCheck = async () => {
    setLoading(true);
    if (modelName) {
      await setOperationInProgress(false);
    } else {
      await checkIfOperationIsFinished(operationId);
    }
    setLoading(false);
  };

  useEffect(() => {
    firstCheck();
  }, []);

  /**
   * Crée le fichier CSV avec les images labellisées
   */
  const createCSV = async () => {
    let CSV = '';
    userImages.forEach((image) => {
      CSV += image.data.fullPath;
      if (image.data.label) {
        CSV += ',';
        CSV += userLabels.find(label => label.labelId === image.data.label).data.name;
      }
      CSV += '\n';
    });

    const contentType = 'text/csv';

    const blob = new Blob([CSV], { type: contentType });

    const CSVUri = await Firebase.addCSVToStorage(blob);

    // On ferme le blob
    blob.close();

    return CSVUri;
  };

  /**
   * Permet d'importer les images labellisées dans le dataset
   */
  const importDatas = async () => {
    if (userLabels.length < 2) {
      Alert.alert(
        Multilingue.translateText('error'),
        Multilingue.translateText('trainModelNotEnoughLabels'),
        [
          {
            text: `${Multilingue.translateText('ok')}`,
            onPress: () => { },
          },
        ],
        { cancelable: false },
      );
      return;
    }

    for (let i = 0; i < userLabels.length; i += 1) {
      const label = userLabels[i].labelId;
      const nbImagesWithLabels = userImages.filter(image => image.data.label === label).length;
      if (nbImagesWithLabels < 10) {
        Alert.alert(
          Multilingue.translateText('error'),
          `${Multilingue.translateText('trainModelNotEnoughImages')} ${userLabels[i].data.name}`,
          [
            {
              text: `${Multilingue.translateText('ok')}`,
              onPress: () => { },
            },
          ],
          { cancelable: false },
        );
        return;
      }
    }

    setOperationInProgress(true);

    const dataset = await GoogleAutoML.createDataset(tokenAutoml, Firebase.getUid());

    Firebase.setAutoMLDataset(dataset.name);

    const csvUri = await createCSV();

    const operationImportData = await GoogleAutoML.importCSVToDataset(tokenAutoml, csvUri, dataset.name);

    Firebase.setAutoMLOperation(operationImportData.name);

    checkIfOperationIsFinished(operationImportData.name);
  };

  /**
   * Permet d'entrainer le modèle
   */
  const trainModel = async () => {
    setOperationInProgress(true);
    const operationModelData = await GoogleAutoML.trainModel(tokenAutoml, datasetName, Firebase.getUid());
    Firebase.setAutoMLOperation(operationModelData.name);
    checkIfOperationIsFinished(operationModelData.name);
  };

  /**
   * Permet de supprimer le dataset
   */
  const deleteDataset = async () => {
    setOperationInProgress(true);
    Firebase.setAutoMLDataset(null);
    Firebase.setAutoMLModel(null);
    const operationDeleteData = await GoogleAutoML.deleteDataset(tokenAutoml, datasetName);

    Firebase.setAutoMLOperation(operationDeleteData.name);
    checkIfOperationIsFinished(operationDeleteData.name);
  };

  /**
   * Confirmation avant de supprimer le dataset
   */
  const checkDeleteDataset = async () => {
    Alert.alert(
      '',
      Multilingue.translateText('checkDeleteDataset'),
      [
        {
          text: Multilingue.translateText('no'),
          onPress: () => { },
        },
        {
          text: Multilingue.translateText('yes'),
          onPress: () => deleteDataset(),
        },
      ],
      { cancelable: true },
    );
  };


  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('trainModelMenu')} />
      <Content contentContainerStyle={styles.contentContainer}>
        {!loading && operationInProgress ? (
          <>
            <Text style={styles.operationTitleText}>{Multilingue.translateText('operationInProgress')}</Text>
            <Text style={styles.operationDetailsText}>{Multilingue.translateText('operationTextDetails')}</Text>
            <Text style={styles.operationDetailsText}>{Multilingue.translateText('operationTextDetails2')}</Text>

            <ActivityIndicator size="large" color="#4B77BE" />
          </>
        )
          : (
            datasetName ? (
              modelName ? (
                <>
                  <Text>{Multilingue.translateText('modelTrained')}</Text>
                  <Button primary style={styles.centeredButton} onPress={checkDeleteDataset} disabled={operationInProgress}>
                    <Text>
                      {Multilingue.translateText('deleteDatasetAndModel')}
                    </Text>
                  </Button>
                </>
              )
                : (
                  <>
                    <Button primary style={styles.centeredButton} onPress={trainModel} disabled={operationInProgress}>
                      <Text>
                        {Multilingue.translateText('trainTheModel')}
                      </Text>
                    </Button>
                    <Button primary style={styles.centeredButton} onPress={checkDeleteDataset} disabled={operationInProgress}>
                      <Text>
                        {Multilingue.translateText('deleteDataset')}
                      </Text>
                    </Button>
                  </>
                )
            )
              : (
                <Button primary style={styles.centeredButton} onPress={importDatas} disabled={operationInProgress}>
                  <Text>
                    {Multilingue.translateText('importDatasButton')}
                  </Text>
                </Button>
              )
          )
        }

      </Content>
    </Container>
  );
}

TrainModelScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  centeredButton: {
    marginTop: 20,
    alignSelf: 'center',
  },
  operationTitleText: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 20,
    textAlign: 'center',
  },
  operationDetailsText: {
    marginBottom: 20,
    textAlign: 'center',
  },
});

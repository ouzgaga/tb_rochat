/**
 * Affiche les images labellisées
 */
import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import ImageGallery from '../components/ImageGallery';
import { AppContext } from '../util/Store';
import HeaderMenu from '../navigation/HeaderMenu';
import Multilingue from '../util/Multilingue';

export default function LabeledImagesScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const { userImages } = appContext;

  const imagesToShow = userImages.filter(image => image.data.label !== null);

  return (
    <>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('labeledImagesMenu')} />
      <ImageGallery images={imagesToShow} />
    </>
  );
}

LabeledImagesScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

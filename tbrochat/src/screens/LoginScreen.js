/**
 * Menu permettant à l'utilisateur de se connecter
 */
import React, { useState, useContext } from 'react';
import {
  StyleSheet,
} from 'react-native';

import {
  Container, Form, Item, Input, Button, Text, Content, Toast,
} from 'native-base';
import * as firebase from 'firebase';
import PropTypes from 'prop-types';

import Multilingue from '../util/Multilingue';
import HeaderMenu from '../navigation/HeaderMenu';
import Logo from '../components/Logo';
import { AppContext } from '../util/Store';

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState('billets@test.com');
  const [password, setPassword] = useState('test1234');

  const appContext = useContext(AppContext);
  const {
    setLoading,
  } = appContext;

  /**
   * Change la valeur de l'email
   * @param {*} value la nouvelle valeur
   */
  const onChangeEmail = (value) => {
    setEmail(value);
  };

  /**
   * Change la valeur du password
   * @param {*} value la nouvelle valeur du password
   */
  const onChangePassword = (value) => {
    setPassword(value);
  };

  /**
   * Connexion de l'utilisateur
   */
  const login = async () => {
    setLoading(true);
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);

      navigation.navigate('Settings');
    } catch (error) {
      const errorCode = error.code;
      if (errorCode === 'auth/invalid-email') {
        Toast.show({
          text: Multilingue.translateText('invalidEmail'),
          type: 'danger',
          duration: 2000,
        });
      } else if (errorCode === 'auth/user-disabled') {
        Toast.show({
          text: Multilingue.translateText('userDisabled'),
          type: 'danger',
          duration: 2000,
        });
      } else if (errorCode === 'auth/user-not-found') {
        Toast.show({
          text: Multilingue.translateText('userNotFound'),
          type: 'danger',
          duration: 2000,
        });
      } else if (errorCode === 'auth/wrong-password') {
        Toast.show({
          text: Multilingue.translateText('wrongPassword'),
          type: 'danger',
          duration: 2000,
        });
      } else {
        Toast.show({
          text: Multilingue.translateText('error'),
          type: 'danger',
          duration: 2000,
        });
      }
    }
    setLoading(false);
  };

  /**
   * Lien pour créer un compte
   */
  const createAnAccount = () => {
    navigation.navigate('SignUp');
  };

  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('loginMenu')} />
      <Content contentContainerStyle={styles.container}>
        <Form>
          <Content contentContainerStyle={styles.centeredContent}>
            <Logo width={150} height={150} />
          </Content>
          <Text style={styles.labelText}>{Multilingue.translateText('email')}</Text>
          <Item regular style={styles.item}>
            <Input placeholder={Multilingue.translateText('email')} value={email} onChangeText={onChangeEmail} />
          </Item>

          <Text style={styles.labelText}>{Multilingue.translateText('password')}</Text>
          <Item regular>
            <Input placeholder={Multilingue.translateText('password')} value={password} onChangeText={onChangePassword} />
          </Item>
        </Form>
        <Button primary style={styles.centeredButton} onPress={login}>
          <Text style={styles.buttonText}>{Multilingue.translateText('login')}</Text>
        </Button>
        <Button transparent onPress={createAnAccount} style={styles.centeredButton}>
          <Text>{Multilingue.translateText('createAccount')}</Text>
        </Button>
      </Content>

    </Container>
  );
}

LoginScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  centeredContent: {
    alignItems: 'center',
  },
  buttonText: {
    textAlign: 'center',
  },
  item: {
    marginBottom: 20,
  },
  labelText: {
    fontSize: 18,
  },
  centeredButton: {
    marginTop: 40,
    alignSelf: 'center',
  },
});


/**
 * Application principale
 * Affiche la réalité augmentée ou la caméra simple selon si l'AR est activée ou non
 */
import React, { useContext } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import { Icon } from 'native-base';
import PropTypes from 'prop-types';


import AR from '../components/AR';
import CameraScreen from '../components/CameraScreen';
import { AppContext } from '../util/Store';

let positionInterval = 0;
let trackingInterval = 0;

export default function HomeScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const { arEnable } = appContext;

  const onClickGalleryButton = async () => {
    setPositionInterval(0);
    setTrackingInterval(0);
    navigation.navigate('ImagePicker');
  };

  const onClickSettingsButton = async () => {
    setPositionInterval(0);
    setTrackingInterval(0);
    navigation.navigate('Settings');
  };

  const setPositionInterval = (interval) => {
    if (positionInterval !== 0) {
      clearInterval(positionInterval);
    }
    positionInterval = interval;
  };

  const setTrackingInterval = (interval) => {
    if (trackingInterval !== 0) {
      clearInterval(trackingInterval);
    }
    trackingInterval = interval;
  };

  return (
    <View style={styles.container}>
      <StatusBar hidden />
      {arEnable ? (
        <AR navigation={navigation} setPositionInterval={setPositionInterval} setTrackingInterval={setTrackingInterval} />
      ) : (
          <CameraScreen navigation={navigation} />
        )}

      <View style={styles.viewButtons}>
        <View
          style={styles.viewButtonsRow}
        >
          <TouchableOpacity onPress={onClickGalleryButton}>
            <Icon
              type="MaterialIcons"
              name="add"
              style={styles.icon}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={onClickSettingsButton}>
            <Icon
              type="MaterialIcons"
              name="settings"
              style={styles.icon}
            />
          </TouchableOpacity>
        </View>
      </View>

    </View>

  );
}

HomeScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewButtons: {
    position: 'absolute',
    top: 20,
    flex: 1,
    padding: 10,
    flexDirection: 'row',
  },
  viewButtonsRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    fontSize: 40,
    color: 'white',
  },
});

module.exports = HomeScreen;

/**
 * Menu avec toutes les images de l'utilisateur
 */
import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import ImageGallery from '../components/ImageGallery';
import { AppContext } from '../util/Store';
import HeaderMenu from '../navigation/HeaderMenu';
import Multilingue from '../util/Multilingue';

export default function AllImagesScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const { userImages } = appContext;

  return (
    <>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('allImagesMenu')} />
      <ImageGallery images={userImages} />
    </>
  );
}

AllImagesScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

/**
 * Permet d'afficher les statistiques du modèle
 */
import React, { useContext, useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import {
  Container, Picker, Content, Text,
} from 'native-base';
import PropTypes from 'prop-types';

import { AppContext } from '../util/Store';
import Multilingue from '../util/Multilingue';
import HeaderMenu from '../navigation/HeaderMenu';
import GoogleAutoML from '../api/GoogleAutoML';
import PercentageCircleComponent from '../components/PercentageCircle';

export default function ModelStatisticsScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const {
    tokenAutoml, setLoading, loading, datasetName, modelName,
  } = appContext;

  const [annotations, setAnnotations] = useState([]);
  const [selectedValue, setSelectedValue] = useState(null);

  const fetchStatistics = async () => {
    if (modelName) {
      setLoading(true);
      const { modelEvaluation } = await GoogleAutoML.getModelStatistics(tokenAutoml, modelName);
      const annotationsArray = [];

      await Promise.all(modelEvaluation.map(async (item) => {
        const { annotationSpecId } = item;
        if (annotationSpecId) {
          const annotationDisplayName = await GoogleAutoML.getLabelDisplayName(tokenAutoml, datasetName, annotationSpecId);
          annotationsArray.push({ annotationSpecId, annotationDisplayName, stats: item });
        } else {
          annotationsArray.push({ annotationSpecId: null, annotationDisplayName: Multilingue.translateText('wholeModel'), stats: item });
        }
      }));

      setAnnotations(annotationsArray);
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchStatistics();
  }, []);

  const getPrecision = () => {
    if (annotations.length) {
      const clickedStats = annotations.find(annotation => annotation.annotationSpecId === selectedValue);
      return clickedStats.stats.classificationEvaluationMetrics.confidenceMetricsEntry[10].precision;
    }
    return 0;
  };

  const getRecall = () => {
    if (annotations.length) {
      const clickedStats = annotations.find(annotation => annotation.annotationSpecId === selectedValue);
      return clickedStats.stats.classificationEvaluationMetrics.confidenceMetricsEntry[10].recall;
    }
    return 0;
  };

  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('modelStatisticsMenu')} />
      <Content contentContainerStyle={styles.container}>
        {
          modelName
            ? (
              !loading && (
                <>
                  <View style={styles.viewPicker}>

                    <Picker
                      note
                      mode="dropdown"
                      style={{ width: '100%' }}
                      selectedValue={selectedValue}
                      onValueChange={(value) => { setSelectedValue(value); }}
                    >
                      {annotations.map(annotation => (<Picker.Item key={annotation.annotationSpecId} label={annotation.annotationDisplayName} value={annotation.annotationSpecId} />))}

                    </Picker>
                  </View>

                  <Text style={styles.labelText}>{Multilingue.translateText('precision')}</Text>
                  <PercentageCircleComponent percent={(getPrecision() * 100).toFixed(0)} />
                  <Text style={styles.labelText}>{Multilingue.translateText('recall')}</Text>
                  <PercentageCircleComponent percent={(getRecall() * 100).toFixed(0)} />
                </>
              )
            )
            : (
              <Content contentContainerStyle={styles.contentStats}>
                <Text style={styles.noModelText}>{Multilingue.translateText('noModel')}</Text>
              </Content>
            )
        }

      </Content>
    </Container>

  );
}

ModelStatisticsScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 20,
    flex: 1,
  },
  viewPicker: {
    width: '100%',
    height: 50,
  },
  contentStats: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  labelText: {
    fontSize: 18,
    marginTop: 20,
  },
  noModelText: {
    textAlign: 'center',
  },
});

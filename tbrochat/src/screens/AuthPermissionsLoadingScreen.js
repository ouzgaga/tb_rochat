/**
 * Check au démarrage de l'application des permissions, du langage et de l'accessibilité à l'AR
 */
import React, { useContext, useEffect } from 'react';
import {
  View,
} from 'react-native';
import { ViroUtils } from 'react-viro';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';
import PropTypes from 'prop-types';

import { AppContext } from '../util/Store';
import CheckPermissions from '../util/CheckPermissions';
import Multilingue from '../util/Multilingue';
import GetBearer from '../constants/GetBearer';


export default function AuthPermissionsLoadingScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const {
    setTokenAutoml,
    setTokenDialogflow,
    setLanguage,
    setAllPermissions,
    setArEnable,
    setArSupported,
    setLoading,
  } = appContext;

  /**
   * lorsque le composant est monté, on appelé initApp qui en fonction des permissions nous redirige vers
   * l'application normale ou les réglages
   */
  useEffect(() => {
    const initApp = async () => {
      setLoading(true);
      await Promise.all(checkIfArIsSupported(), checkDefaultLanguage(), getBearer());
      setLoading(false);
      const allPermissions = await checkPermissions();
      if (allPermissions) {
        navigation.navigate('Home');
      } else {
        navigation.navigate('Settings');
      }
    };

    initApp();
  }, []);

  /**
   * Check les permissions de l'application
   */
  const checkPermissions = async () => {
    const permissions = await CheckPermissions();
    setAllPermissions(await CheckPermissions());
    return permissions;
  };

  /**
   * Check si l'AR est supporté ou non
   */
  const checkIfArIsSupported = async () => {
    ViroUtils.isARSupportedOnDevice(notSupportedAR, supportedAR);
  };

  /**
   * Si l'AR n'est pas supporté
   */
  const notSupportedAR = () => {
    setArEnable(false);
    setArSupported(false);
  };

  /**
   * Si l'AR est supporté
   */
  const supportedAR = () => {
    setArEnable(true);
    setArSupported(true);
  };

  /**
   * Check le langage de l'application
   */
  const checkDefaultLanguage = async () => {
    const stockedLanguage = await AsyncStorage.getItem('language');
    if (stockedLanguage) {
      setLanguage(stockedLanguage);
      Multilingue.setLocale(stockedLanguage);
    } else {
      const locales = RNLocalize.getLocales(); // retourne les languages de prédilection de l'utilisateur
      const favoriteLanguage = locales[0].languageCode.startsWith('fr') ? 'fr' : 'en';
      setLanguage(favoriteLanguage);
      Multilingue.setLocale(favoriteLanguage);
    }
  };

  /**
   * Récupère les tokens OAuth pour communiquer avec Google Cloud AutoML et Dialogflow
   */
  const getBearer = async () => {
    setTokenAutoml(await GetBearer.getBearerAutoML());
    setTokenDialogflow(await GetBearer.getBearerDialogflow());
  };

  return (
    <View />
  );
}

AuthPermissionsLoadingScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

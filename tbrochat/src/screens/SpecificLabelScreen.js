/**
 * Galerie pour les labels créés
 */
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'native-base';

import ImageGallery from '../components/ImageGallery';
import { AppContext } from '../util/Store';
import HeaderLabelMenu from '../navigation/HeaderLabelMenu';

let rerender = true;

export default function SpecificLabelScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const { userImages } = appContext;

  const labelSelected = navigation.getParam('labelSelected');

  rerender = !rerender;

  const imagesToShow = userImages.filter(image => image.data.label === labelSelected.labelId);


  return (
    <Container key={labelSelected.labelId}>
      <HeaderLabelMenu navigation={navigation} label={labelSelected} images={imagesToShow} />
      <ImageGallery images={imagesToShow} labelSelected={labelSelected} />
    </Container>
  );
}

SpecificLabelScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

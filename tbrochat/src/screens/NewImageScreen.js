/**
 * Permet d'ajouter de noouvelles images ou photographies afin d'entrainer le modèle
 */
import React, { useState, useContext } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import PropTypes from 'prop-types';

import {
  Container, Content, Picker, Button, Toast, Text,
} from 'native-base';

import Firebase from '../api/Firebase';
import { AppContext } from '../util/Store';
import Multilingue from '../util/Multilingue';
import HeaderMenu from '../navigation/HeaderMenu';
import PercentageCircleComponent from '../components/PercentageCircle';

export default function NewImageScreen({ navigation }) {
  const [selectedLabel, setSelectedLabel] = useState(null);
  const [nbUpload, setNbUpload] = useState(0);
  const [nbToUpload, setNbToUpload] = useState(0);
  const appContext = useContext(AppContext);
  const { userLabels } = appContext;

  /**
   * Permet de choisir plusieurs images de la galerie
   */
  const pickMultipleImages = () => {
    ImagePicker.openPicker({
      multiple: true,
      compressImageQuality: 0.5,
    }).then(async (images) => {
      setNbToUpload(images.length);
      let cmp = 0;
      await Promise.all(images.map(async (image) => {
        const ref = await uploadImageAsync(image.path);
        await Firebase.addImage(ref.fullPath, ref.downloadLink, selectedLabel);
        cmp += 1;
        await setNbUpload(cmp);
      }));
      Toast.show({
        text: Multilingue.translateText('uploadFinished'),
        type: 'success',
        duration: 2000,
      });
      setTimeout(() => {
        setNbUpload(0);
        setNbToUpload(0);
      }, 2000);
    })
    .catch(() => {});
  };

  /**
   * Permet de prendre une photographie de l'objet
   */
  const takePicture = () => {
    ImagePicker.openCamera({
      compressImageQuality: 0.5,
    }).then(async (image) => {
      setNbToUpload(1);
      const ref = await uploadImageAsync(image.path);
      await Firebase.addImage(ref.fullPath, ref.downloadLink, selectedLabel);
      await setNbUpload(1);
      Toast.show({
        text: Multilingue.translateText('uploadFinished'),
        type: 'success',
        duration: 2000,
      });
      setTimeout(() => {
        setNbUpload(0);
        setNbToUpload(0);
      }, 2000);
    })
    .catch(() => {});
  };

  // https://github.com/expo/expo/issues/2402#issuecomment-443726662
  const uploadImageAsync = async (uri) => {
    // Why are we using XMLHttpRequest? See:
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        console.log(e);
        reject(new TypeError('Network request failed'));
      };
      xhr.responseType = 'blob';
      xhr.open('GET', uri, true);
      xhr.send(null);
    });

    const ref = await Firebase.addImageToStorage(blob);

    blob.close();

    return ref;
  };

  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('newImageMenu')} />
      <Content contentContainerStyle={styles.container}>
        {nbUpload === nbToUpload ? (
          <>
            <View style={styles.viewPicker}>
              <Picker
                note
                mode="dropdown"
                style={styles.picker}
                selectedValue={selectedLabel}
                onValueChange={(value) => { setSelectedLabel(value); }}
              >
                {userLabels.map(label => (<Picker.Item key={label.labelId} label={label.data.name} value={label.labelId} />))}

                <Picker.Item label={Multilingue.translateText('unlabeled')} value={null} />

              </Picker>
            </View>

            <Button primary style={styles.centeredButton} onPress={pickMultipleImages}>
              <Text>{Multilingue.translateText('imageFromGallery')}</Text>
            </Button>
            <Button primary style={styles.centeredButton} onPress={takePicture}>
              <Text style={styles.buttonText}>{Multilingue.translateText('imageFromCamera')}</Text>
            </Button>
          </>
        )
          : (
            <>
              <Text style={styles.uploadText}>{Multilingue.translateText('uploadImageInProgress')}</Text>
              <Text style={styles.uploadText}>{`${nbUpload} / ${nbToUpload}`}</Text>
              <PercentageCircleComponent percent={((nbUpload / nbToUpload) * 100).toFixed(0)} />
            </>
          )}

      </Content>
    </Container>
  );
}

NewImageScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewPicker: {
    width: '100%',
    height: 50,
  },
  picker: {
    width: '100%',
    height: 50,
  },
  centeredButton: {
    alignSelf: 'center',
    marginTop: 20,
  },
  buttonText: {
    textAlign: 'center',
  },
  uploadText: {
    marginVertical: 10,
    fontSize: 18,
  },
});

/**
 * Permet à un utilisateur de créer un compte
 */
import React, { useState, useContext } from 'react';
import {
  StyleSheet,
} from 'react-native';

import {
  Container, Form, Item, Input, Button, Text, Content, Toast,
} from 'native-base';
import * as firebase from 'firebase';
import PropTypes from 'prop-types';

import Multilingue from '../util/Multilingue';
import HeaderMenu from '../navigation/HeaderMenu';
import Logo from '../components/Logo';
import { AppContext } from '../util/Store';

export default function SignUpScreen({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const appContext = useContext(AppContext);
  const {
    setLoading,
  } = appContext;

  /**
   * Lorsque l'email change
   * @param {*} value la nouvelle valeur de l'email
   */
  const onChangeEmail = (value) => {
    setEmail(value);
  };

  /**
   * Lorsque le mot de passe change
   * @param {*} value la nouvelle valeur de l'email
   */
  const onChangePassword = (value) => {
    setPassword(value);
  };

  /**
   * Permet de créer un nouvel utilisateur
   */
  const signUp = async () => {
    setLoading(true);
    try {
      await firebase.auth().createUserWithEmailAndPassword(email, password);

      navigation.navigate('Settings');
    } catch (error) {
      const errorCode = error.code;
      if (errorCode === 'auth/email-already-in-use') {
        Toast.show({
          text: Multilingue.translateText('emailAlreadyInUse'),
          type: 'danger',
          duration: 2000,
        });
      } else if (errorCode === 'auth/invalid-email') {
        Toast.show({
          text: Multilingue.translateText('invalidEmail'),
          type: 'danger',
          duration: 2000,
        });
      } else if (errorCode === 'auth/operation-not-allowed') {
        Toast.show({
          text: Multilingue.translateText('operationNotAllowed'),
          type: 'danger',
          duration: 2000,
        });
      } else if (errorCode === 'auth/weak-password') {
        Toast.show({
          text: Multilingue.translateText('weakPassword'),
          type: 'danger',
          duration: 2000,
        });
      } else {
        Toast.show({
          text: Multilingue.translateText('error'),
          type: 'danger',
          duration: 2000,
        });
      }
    }
    setLoading(false);
  };

  /**
   * Lien pour se connecter
   */
  const createAnAccount = () => {
    navigation.navigate('Login');
  };

  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('signUpMenu')} />
      <Content contentContainerStyle={styles.container}>
        <Form>
          <Content contentContainerStyle={styles.centeredContent}>
            <Logo width={150} height={150} />
          </Content>
          <Text style={styles.labelText}>{Multilingue.translateText('email')}</Text>
          <Item regular style={styles.item}>
            <Input placeholder={Multilingue.translateText('email')} value={email} onChangeText={onChangeEmail} />
          </Item>

          <Text style={styles.labelText}>{Multilingue.translateText('password')}</Text>
          <Item regular>
            <Input placeholder={Multilingue.translateText('password')} value={password} onChangeText={onChangePassword} />
          </Item>
        </Form>
        <Button primary style={styles.centeredButton} onPress={signUp}>
          <Text style={styles.buttonText}>{Multilingue.translateText('createAccount')}</Text>
        </Button>
        <Button transparent onPress={createAnAccount} style={styles.centeredButton}>
          <Text>{Multilingue.translateText('alreadyAccount')}</Text>
        </Button>
      </Content>

    </Container>
  );
}

SignUpScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  centeredContent: {
    alignItems: 'center',
  },
  buttonText: {
    textAlign: 'center',
  },
  item: {
    marginBottom: 20,
  },
  labelText: {
    fontSize: 18,
  },
  centeredButton: {
    marginTop: 40,
    alignSelf: 'center',
  },
});

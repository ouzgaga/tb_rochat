
/**
 * Ce fichier permet d'analyser des images de la galerie
 */
import React, { useState, useContext } from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  View,
} from 'react-native';
import {
  Container,
  Content,
  Picker,
  Button,
  Text,
} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import Tts from 'react-native-tts';
import PropTypes from 'prop-types';

import Multilingue from '../util/Multilingue';
import ImageLocalisationResults from '../components/ImageLocalisationResults';
import { AppContext } from '../util/Store';
import HeaderMenu from '../navigation/HeaderMenu';
import FullScreenBackgroundImage from '../components/FullScreenBackgroundImage';
import FetchVision from '../util/FetchVision';
import GoogleAutoML from '../api/GoogleAutoML';
import ImageClassifictionResults from '../components/ImageClassificationResults';

export default function ImagePickerScreen({ navigation }) {
  const [selectedMode, setSelectedMode] = useState(0);
  const [imagePicked, setImagePicked] = useState(null);
  const [resultsObjectDetection, setResultsObjectDetection] = useState([]);
  const [classificationResults, setClassificationResults] = useState([]);

  /**
   * Utilisation du contexte
   */
  const appContext = useContext(AppContext);
  const {
    language, tokenAutoml, setLoading, modelName,
  } = appContext;

  /**
   * Initialise le Text to Speech
   */
  Tts.getInitStatus().then(() => {
    Tts.setDefaultLanguage(language);
  }, (err) => {
    if (err.code === 'no_engine') {
      Tts.requestInstallEngine();
    }
  });

  /**
   * Analyse d'une image selon le mode sélectionné
   */
  const analyseImage = async () => {
    try {
      const image = await ImagePicker.openPicker({
        compressImageQuality: 0.5,
        includeBase64: true,
      });

      setLoading(true);
      setImagePicked({ data: image.data, mime: image.mime });

      if (selectedMode === 0) {
        try {
          Tts.speak(`${Multilingue.translateText('analyseInProgress')}, mode ${Multilingue.translateText('Localization')}`);
        } catch (e) {
          console.log(e);
        }
        await fetchGoogleForLocalization(image);
      } else if (selectedMode === 1) {
        Tts.speak(`${Multilingue.translateText('analyseInProgress')}, mode ${Multilingue.translateText('Classification')}`);
        await fetchGoogleForClassification(image);
      } else if (selectedMode === 2) {
        Tts.speak(`${Multilingue.translateText('analyseInProgress')}, mode ${Multilingue.translateText('Personalization')}`);
        await fetchGoogleForPersonalization(image);
      }
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };

  /**
   * Utilisation du mode de localisation
   */
  const fetchGoogleForLocalization = async (image) => {
    if (image) {
      try {
        const results = await FetchVision.fetchGoogleVisionAndTranslateMode(0, image.data);
        if (results.length === 0) {
          Tts.speak(Multilingue.translateText('noneDetectedObject'));
        } else {
          setResultsObjectDetection(results);
          results.forEach((objectDetected) => {
            const { name, score } = objectDetected;
            Tts.speak(`${name}, ${score} ${Multilingue.translateText('percent')}`);
          });
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  /**
   * Utilisaion du mode de classification
   */
  const fetchGoogleForClassification = async (image) => {
    if (image) {
      try {
        const results = await FetchVision.fetchGoogleVisionAndTranslateMode(1, image.data);
        if (results.length === 0) {
          Tts.speak(Multilingue.translateText('noneDetectedObject'));
        } else {
          setClassificationResults(results);
          results.forEach((objectDetected) => {
            const { name, score } = objectDetected;

            Tts.speak(`${name}, ${score} ${Multilingue.translateText('percent')}`);
          });
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  /**
   * Utilisation du mode de personalisation
   */
  const fetchGoogleForPersonalization = async (image) => {
    if (image) {
      try {
        const results = await GoogleAutoML.imagePrediction(tokenAutoml, modelName, image.data);
        setClassificationResults(results);
        if (results.length === 0) {
          Tts.speak(Multilingue.translateText('noneDetectedObject'));
        } else {
          setClassificationResults(results);
          results.forEach((objectDetected) => {
            const nameToSpeech = objectDetected.name.replace(/_/g, ' ');
            const { score } = objectDetected;

            Tts.speak(`${nameToSpeech}, ${score} ${Multilingue.translateText('percent')}`);
          });
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  /**
   * Suppression des résultats
   */
  const deleteResults = () => {
    setImagePicked(null);
    setClassificationResults([]);
    setResultsObjectDetection([]);
  };

  /**
   * Permet de retourner le nom du mode actuellement choisit
   * @param {*} mode l'tidentifiant du mode
   */
  const getModeName = (mode) => {
    if (mode === 0) {
      return Multilingue.translateText('Localization');
    }
    if (mode === 1) {
      return Multilingue.translateText('Classification');
    }
    if (mode === 2) {
      return Multilingue.translateText('Personalization');
    }
    return '';
  };

  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('imageFromGalleryMenu')} />
      {imagePicked ? (
        <TouchableHighlight
          style={styles.container}
          onPress={deleteResults}
        >
          <>
            <FullScreenBackgroundImage imagePicked={imagePicked} />
            <ImageLocalisationResults resultsObjectDetection={resultsObjectDetection} />
            <ImageClassifictionResults classificationResults={classificationResults} />
          </>
        </TouchableHighlight>
      )
        : (

          <Content contentContainerStyle={styles.contentContainer}>
            <>
              <View style={styles.viewPicker}>
                {modelName ? (
                  <Picker
                    note
                    mode="dropdown"
                    style={styles.picker}
                    selectedValue={selectedMode}
                    onValueChange={(value) => { setSelectedMode(value); }}
                  >
                    <Picker.Item label={Multilingue.translateText('Localization')} value={0} />
                    <Picker.Item label={Multilingue.translateText('Classification')} value={1} />
                    <Picker.Item label={Multilingue.translateText('Personalization')} value={2} />

                  </Picker>
                ) : (
                    <Picker
                      note
                      mode="dropdown"
                      style={styles.picker}
                      selectedValue={selectedMode}
                      onValueChange={(value) => { setSelectedMode(value); }}
                    >
                      <Picker.Item label={Multilingue.translateText('Localization')} value={0} />
                      <Picker.Item label={Multilingue.translateText('Classification')} value={1} />

                    </Picker>
                  )}

              </View>
              <Button primary style={styles.centeredButton} onPress={analyseImage}>
                <Text style={styles.buttonText}>{`${Multilingue.translateText('selectImageFromGallery')} ${getModeName(selectedMode)}`}</Text>
              </Button>
            </>

          </Content>
        )}
    </Container>
  );
}

ImagePickerScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewPicker: {
    width: '100%',
    height: 50,
    marginBottom: 20,
  },
  picker: {
    width: '100%',
  },
  buttonText: {
    textAlign: 'center',
  },
  centeredButton: {
    marginTop: 20,
    alignSelf: 'center',
  },
});

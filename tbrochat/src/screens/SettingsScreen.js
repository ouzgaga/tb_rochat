/**
 * Menu des réglages, permet de changer la langue, gérer les permissions,
 * ainsi que la réalité augmentée.
 */

import React, { useContext } from 'react';
import {
  View, Picker, StyleSheet, Switch, StatusBar, Alert,
} from 'react-native';
import {
  Container, Button, Text, Content,
} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import PropTypes from 'prop-types';

import Multilingue from '../util/Multilingue';
import CheckPermissions from '../util/CheckPermissions';
import HeaderMenu from '../navigation/HeaderMenu';
import { AppContext } from '../util/Store';
import Firebase from '../api/Firebase';


export default function SettingsScreen({ navigation }) {
  const appContext = useContext(AppContext);
  const {
    language, setLanguage, arEnable, arSupported, allPermissions, setArEnable, setAllPermissions, user, setLoading,
  } = appContext;

  /**
   * Permet de changer la langue de l'application
   */
  const changeLanguage = async (newLanguage) => {
    Multilingue.setLocale(newLanguage); // changer la langue pour i18n-js
    setLanguage(newLanguage); // change l'état global
    try {
      await AsyncStorage.setItem('language', newLanguage); // enregistre la nouvelle langue de manière persistante
    } catch (error) {
      // Error saving data
    }
  };

  /**
   * Activation ou dösactivation de la réalité augmentée
   */
  const toogleArEnable = (value) => {
    setArEnable(value);
  };

  /**
   * Check toutes les permissions nécessaires à l'applications
   */
  const checkPermissions = async () => {
    setAllPermissions(await CheckPermissions());
  };

  /**
   * Confirmation si l'utilisateur veut bien se déconnecter
   */
  const checkDisconnect = () => {
    Alert.alert(
      '',
      Multilingue.translateText('checkDisconnectUser'),
      [
        {
          text: Multilingue.translateText('no'),
          onPress: () => { },
        },
        {
          text: Multilingue.translateText('yes'),
          onPress: () => disconnectUser(),
        },
      ],
      { cancelable: true },
    );
  };

  /**
   * Quitter les réglages
   */
  const exitSettings = () => {
    navigation.navigate('Home');
  };

  /**
   * Permet à l'utilisateur de se déconnecter
   */
  const disconnectUser = async () => {
    setLoading(true);
    await Firebase.signOut();
    setLoading(false);
  };

  return (
    <Container>
      <HeaderMenu navigation={navigation} title={Multilingue.translateText('settingsMenu')} />

      <Content contentContainerStyle={styles.container}>
        <StatusBar barStyle="default" />

        <View style={styles.settingsView}>
          <View style={styles.settingItemView}>
            <Text style={styles.labelText}>{Multilingue.translateText('language')}</Text>
            <View style={styles.borderView}>
              <Picker
                style={styles.picker}
                selectedValue={language}
                onValueChange={changeLanguage}
              >
                <Picker.Item label="English" value="en" />
                <Picker.Item label="Français" value="fr" />
              </Picker>
            </View>
          </View>
          <View style={styles.settingItemView}>
            <Text style={styles.labelText}>{Multilingue.translateText('arLabel')}</Text>
            <Switch
              onValueChange={toogleArEnable}
              value={arEnable}
              disabled={!arSupported}
            />
          </View>
          <View style={styles.settingItemView}>
            <Text style={styles.labelText}>{Multilingue.translateText('permissionsLabel')}</Text>
            <Switch
              style={styles.switch}
              onValueChange={checkPermissions}
              value={allPermissions}
              disabled={allPermissions}
            />
          </View>
          <Button primary style={styles.centeredButton} onPress={exitSettings}>
            <Text>{Multilingue.translateText('exitSettingMenu')}</Text>
          </Button>
          {user && (
            <Button primary style={styles.centeredButton} onPress={checkDisconnect}>
              <Text>{Multilingue.translateText('signOut')}</Text>
            </Button>
          )}
        </View>
      </Content>
    </Container>
  );
}

SettingsScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  titleView: {
    marginTop: 30,
    alignItems: 'center',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  settingsView: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  settingItemView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
  },
  labelText: {
    fontSize: 18,
  },
  borderView: {
    borderRadius: 10,
    borderWidth: 0.5,
  },
  picker: {
    width: 130,
  },
  centeredButton: {
    marginTop: 20,
    alignSelf: 'center',
  },
});

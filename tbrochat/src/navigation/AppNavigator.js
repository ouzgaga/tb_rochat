/**
 * Contient la naviagtion entre les différents menus.
 * Décrit également le drawer pour le menu des réglages
 */
import {
  createSwitchNavigator, createAppContainer, createDrawerNavigator,
} from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/SettingsScreen';
import AuthPermissionsLoadingScreen from '../screens/AuthPermissionsLoadingScreen';

import ImagePickerScreen from '../screens/ImagePickerScreen';
import NewLabelScreen from '../screens/NewLabelScreen';
import NewImageScreen from '../screens/NewImageScreen';
import AllImagesScreen from '../screens/AllImagesScreen';
import LabeledImagesScreen from '../screens/LabeledImagesScreen';
import UnlabeledImagesScreen from '../screens/UnlabeledImagesScreen';
import SpecificLabelScreen from '../screens/SpecificLabelScreen';
import ModelStatisticsScreen from '../screens/ModelStatisticsScreen';
import TrainModelScreen from '../screens/TrainModelScreen';

import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';


import SettingsMenu from './SettingsMenu';


const SettingsNavigator = createDrawerNavigator({
  Settings: SettingsScreen,
  ImagePicker: ImagePickerScreen,
  NewImage: NewImageScreen,
  NewLabel: NewLabelScreen,
  AllImages: AllImagesScreen,
  LabeledImages: LabeledImagesScreen,
  UnlabeledImages: UnlabeledImagesScreen,
  SpecificLabel: SpecificLabelScreen,
  ModelStatistics: ModelStatisticsScreen,
  TrainModel: TrainModelScreen,
  Login: LoginScreen,
  SignUp: SignUpScreen,
},
  {
    initialRouteName: 'Settings',
    drawerPosition: 'left',
    contentComponent: SettingsMenu,
  });

export default createAppContainer(createSwitchNavigator(
  {
    Home: HomeScreen,
    AuthPermissionsLoading: AuthPermissionsLoadingScreen,
    Settings: SettingsNavigator,
  },
  {
    initialRouteName: 'AuthPermissionsLoading',
  },
));

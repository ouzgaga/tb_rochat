/**
 * Le header pour les labels spécifiques
 */

import React, { useState, useContext } from 'react';
import {
  Alert,
  StyleSheet,
} from 'react-native';

import PropTypes from 'prop-types';

import {
  Header, Left, Right, Body, Icon, Text, Input, Item,
} from 'native-base';
import { AppContext } from '../util/Store';
import Firebase from '../api/Firebase';
import Multilingue from '../util/Multilingue';

export default function HeaderLabelMenu({ navigation, label, images }) {
  const appContext = useContext(AppContext);
  const { userLabels } = appContext;

  const [labelName, setLabelName] = useState(label.data.name);
  const [editMode, setEditMode] = useState(false);

  /**
   * Permet de modifier le label
   */
  const editLabel = () => {
    setEditMode(true);
  };

  /**
   * Permet d'annuler la modification du label
   */
  const closeEditLabel = () => {
    setEditMode(false);
    navigation.navigate('SpecificLabel', {
      labelSelected: { labelId: label.labelId, data: { name: labelName } },
    });
  };

  /**
   * Permet de supprimer un label
   */
  const deleteLabel = () => {
    Alert.alert(
      '',
      Multilingue.translateText('checkDeleteLabel'),
      [
        {
          text: Multilingue.translateText('no'),
          onPress: () => { },
        },
        {
          text: Multilingue.translateText('yes'),
          onPress: () => {
            images.forEach(image => Firebase.changeLabelForImage(image.imageId, null));
            Firebase.deleteLabel(label.labelId);
            navigation.navigate('Settings');
          },
        },
      ],
      { cancelable: false },
    );
  };

  /**
   * Check si le nouveau label respecte les expressions regex et qu'il ne correspond pas à un label existant
   * Si tel est le cas, modifie le label
   */
  const validateEditLabel = () => {
    const re = /^[a-zA-Z0-9]\w*$/;
    if (re.test(labelName)) {
      for (let i = 0; i < userLabels.length; i += 1) {
        if (userLabels[i].data.name === labelName) {
          Alert.alert(
            Multilingue.translateText('error'),
            Multilingue.translateText('alreadyExistingLabel'),
            [
              {
                text: Multilingue.translateText('ok'),
                onPress: () => { },
              },
            ],
            { cancelable: false },
          );
          return;
        }
      }

      Alert.alert(
        '',
        Multilingue.translateText('checkChangeLabelName'),
        [
          {
            text: Multilingue.translateText('no'),
            onPress: () => { },
          },
          {
            text: Multilingue.translateText('yes'),
            onPress: () => {
              Firebase.changeLabelName(label.labelId, labelName);
              closeEditLabel();
            },
          },
        ],
        { cancelable: false },
      );
    } else {
      Alert.alert(
        Multilingue.translateText('errorMalformatedLabel'),
        Multilingue.translateText('malformatedLabelExplain'),
        [
          {
            text: Multilingue.translateText('ok'),
            onPress: () => { },
          },
        ],
        { cancelable: false },
      );
    }
  };

  /**
   * Lorsque l'utilisateur modifie le nom du label sur son clavir
   * @param {*} value la valeur actuelle du nouveau label
   */
  const textLabelChange = (value) => {
    setLabelName(value);
  };


  return (
    <Header>
      {editMode ? (
        <>
          <Left>
            <Icon style={styles.headerColor} name="ios-menu" onPress={() => navigation.toggleDrawer()} />
          </Left>
          <Body>
            <Item>
              <Input style={[styles.headerColor, { width: '100%' }]} placeholder={Multilingue.translateText('labelNamePlaceholder')} value={labelName} onChangeText={textLabelChange} />
            </Item>
          </Body>
          <Right>
            <Icon style={[styles.headerColor, { paddingRight: 20 }]} name="checkmark" onPress={() => validateEditLabel()} />
            <Icon style={styles.headerColor} name="close" onPress={() => closeEditLabel()} />
          </Right>
        </>
      ) : (
          <>
            <Left>
              <Icon style={styles.headerColor} name="ios-menu" onPress={() => navigation.toggleDrawer()} />
            </Left>
            <Body>
              <Text style={styles.headerColor}>{label.data.name}</Text>
            </Body>
            <Right>
              <Icon style={[styles.headerColor, { paddingRight: 20 }]} name="create" onPress={() => editLabel()} />
              <Icon style={styles.headerColor} name="trash" onPress={() => deleteLabel()} />
            </Right>
          </>
        )
      }
    </Header>
  );
}

HeaderLabelMenu.propTypes = {
  navigation: PropTypes.shape().isRequired,
  label: PropTypes.shape().isRequired,
  images: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerColor: {
    color: 'white',
  },
});

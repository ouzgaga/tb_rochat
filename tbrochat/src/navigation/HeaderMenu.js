/**
 * Le header des différents menus des réglages
 */

import React from 'react';
import {
  StyleSheet,
} from 'react-native';

import PropTypes from 'prop-types';

import {
  Header, Left, Body, Icon, Text,
} from 'native-base';

export default function HeaderMenu({ navigation, title }) {
  return (
    <Header>
      <Left>
        <Icon style={styles.headerColor} name="ios-menu" onPress={() => navigation.toggleDrawer()} />
      </Left>
      <Body>
        <Text style={styles.headerColor}>{title}</Text>
      </Body>
    </Header>
  );
}

HeaderMenu.propTypes = {
  navigation: PropTypes.shape().isRequired,
  title: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  headerColor: {
    color: 'white',
  },
});

/**
 * Mise en forme du drawer menu pour les régalages
 */
import React, { useContext, useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';

import TextLinesOnSides from '../components/TextLinesOnSides';
import Multilingue from '../util/Multilingue';
import { AppContext } from '../util/Store';

export default function SettingsMenu({ navigation }) {
  const [selectedMenu, setSelectedMenu] = useState('Settings');

  const appContext = useContext(AppContext);
  const {
    userLabels, allPermissions, user, userImages,
  } = appContext;

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <Text
          onPress={() => { navigation.navigate('Settings'); setSelectedMenu('Settings'); }}
          style={selectedMenu === 'Settings' ? styles.selected : styles.item}
        >
          {Multilingue.translateText('settingsMenu')}
        </Text>

        {allPermissions
          && (
            <>
              <Text
                onPress={() => { navigation.navigate('ImagePicker'); setSelectedMenu('ImagePicker'); }}
                style={selectedMenu === 'ImagePicker' ? styles.selected : styles.item}
              >
                {Multilingue.translateText('imageFromGalleryMenu')}
              </Text>
              <Text
                onPress={() => { navigation.navigate('Home'); setSelectedMenu('Home'); }}
                style={selectedMenu === 'Home' ? styles.selected : styles.item}
              >
                {Multilingue.translateText('exitSettingMenu')}
              </Text>
              <TextLinesOnSides title={Multilingue.translateText('personalizationLabel')} textSize={16} />

              {user ? (
                <View
                  style={styles.personalizationView}
                >
                  <Text
                    onPress={() => { navigation.navigate('NewLabel'); setSelectedMenu('NewLabel'); }}
                    style={selectedMenu === 'NewLabel' ? styles.selectedItemPersonalization2 : styles.itemPersonalization2}
                  >
                    {Multilingue.translateText('newLabelMenu')}
                  </Text>
                  <Text
                    onPress={() => { navigation.navigate('NewImage'); setSelectedMenu('NewImage'); }}
                    style={selectedMenu === 'NewImage' ? styles.selectedItemPersonalization2 : styles.itemPersonalization2}
                  >
                    {Multilingue.translateText('newImageMenu')}
                  </Text>
                  <TextLinesOnSides title={Multilingue.translateText('imagesLabel')} textSize={14} />

                  <TouchableOpacity
                    onPress={() => { navigation.navigate('AllImages'); setSelectedMenu('AllImages'); }}
                    style={selectedMenu === 'AllImages' ? styles.selectedItemPersonalizationWithNumber : styles.itemPersonalizationWithNumber}
                  >
                    <>
                      <Text
                        style={selectedMenu === 'AllImages' ? styles.selectedItemPersonalization : styles.itemPersonalization}
                      >
                        {Multilingue.translateText('allImagesMenu')}
                      </Text>

                      <Text>
                        {userImages.length}
                      </Text>
                    </>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => { navigation.navigate('LabeledImages'); setSelectedMenu('LabeledImages'); }}
                    style={selectedMenu === 'LabeledImages' ? styles.selectedItemPersonalizationWithNumber : styles.itemPersonalizationWithNumber}
                  >
                    <>
                      <Text
                        style={selectedMenu === 'LabeledImages' ? styles.selectedItemPersonalization : styles.itemPersonalization}
                      >
                        {Multilingue.translateText('labeledImagesMenu')}

                      </Text>
                      <Text>
                        {userImages.filter(image => image.data.label !== null).length}
                      </Text>
                    </>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => { navigation.navigate('UnlabeledImages'); setSelectedMenu('UnlabeledImages'); }}
                    style={selectedMenu === 'UnlabeledImages' ? styles.selectedItemPersonalizationWithNumber : styles.itemPersonalizationWithNumber}
                  >
                    <>
                      <Text
                        style={selectedMenu === 'UnlabeledImages' ? styles.selectedItemPersonalization : styles.itemPersonalization}
                      >
                        {Multilingue.translateText('unlabeledImagesMenu')}

                      </Text>
                      <Text>
                        {userImages.filter(image => image.data.label === null).length}
                      </Text>
                    </>
                  </TouchableOpacity>

                  <TextLinesOnSides title={Multilingue.translateText('labelsLabel')} textSize={14} />

                  {
                    userLabels.map(label => (
                      <TouchableOpacity
                        key={label.labelId}
                        style={selectedMenu === label.data.name ? styles.selectedItemPersonalizationWithNumber : styles.itemPersonalizationWithNumber}
                        onPress={() => {
                          setSelectedMenu(label.data.name);
                          navigation.closeDrawer();
                          navigation.navigate('SpecificLabel', {
                            labelSelected: label,
                          });
                        }}
                      >
                        <>
                          <Text
                            style={selectedMenu === label.data.name ? styles.selectedItemPersonalization : styles.itemPersonalization}
                          >
                            {label.data.name}
                          </Text>
                          <Text>
                            {userImages.filter(image => image.data.label === label.labelId).length}
                          </Text>
                        </>
                      </TouchableOpacity>
                    ))
                  }

                  <TextLinesOnSides title={Multilingue.translateText('modelLabel')} textSize={14} />

                  <Text
                    onPress={() => { navigation.navigate('ModelStatistics'); setSelectedMenu('ModelStatistics'); }}
                    style={selectedMenu === 'ModelStatistics' ? styles.selectedItemPersonalization2 : styles.itemPersonalization2}

                  >
                    {Multilingue.translateText('modelStatisticsMenu')}
                  </Text>
                  <Text
                    onPress={() => { navigation.navigate('TrainModel'); setSelectedMenu('TrainModel'); }}
                    style={selectedMenu === 'TrainModel' ? styles.selectedItemPersonalization2 : styles.itemPersonalization2}
                  >
                    {Multilingue.translateText('trainModelMenu')}
                  </Text>
                </View>
              ) : (
                  <>
                    <Text
                      onPress={() => { navigation.navigate('Login'); setSelectedMenu('Login'); }}
                      style={selectedMenu === 'Login' ? styles.selectedItemPersonalization2 : styles.itemPersonalization2}
                    >
                      {Multilingue.translateText('loginMenu')}
                    </Text>
                    <Text
                      onPress={() => { navigation.navigate('SignUp'); setSelectedMenu('SignUp'); }}
                      style={selectedMenu === 'SignUp' ? styles.selectedItemPersonalization2 : styles.itemPersonalization2}
                    >
                      {Multilingue.translateText('createAccount')}
                    </Text>
                  </>
                )}
            </>
          )}
      </ScrollView>
    </SafeAreaView>
  );
}


SettingsMenu.propTypes = {
  navigation: PropTypes.shape().isRequired,
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 14,
    paddingLeft: 20,
    paddingTop: 10,
    fontWeight: 'bold',
  },
  menuView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  item: {
    padding: 20,
    fontWeight: 'bold',
  },
  selected: {
    padding: 20,
    fontWeight: 'bold',
    color: '#4B77BE',
    backgroundColor: 'rgba(230, 230, 230, 1)',
  },
  itemPersonalization: {
    fontWeight: 'bold',
  },
  selectedItemPersonalization: {
    fontWeight: 'bold',
    color: '#4B77BE',
  },
  itemPersonalization2: {
    paddingVertical: 20,
    paddingRight: 20,
    paddingLeft: 40,
    fontWeight: 'bold',
  },
  selectedItemPersonalization2: {
    paddingVertical: 20,
    paddingRight: 20,
    paddingLeft: 40,
    fontWeight: 'bold',
    color: '#4B77BE',
    backgroundColor: 'rgba(230, 230, 230, 1)',
  },
  loginView: {
    paddingLeft: 20,
  },
  titleLabel: {
    paddingTop: 10,
    fontSize: 14,
  },
  selectedItemPersonalizationWithNumber: {
    paddingVertical: 20,
    paddingRight: 20,
    paddingLeft: 40,
    backgroundColor: 'rgba(230, 230, 230, 1)',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemPersonalizationWithNumber: {
    paddingVertical: 20,
    paddingRight: 20,
    paddingLeft: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

/**
 * Permet d'afficher un loader pour le chargement
 */
import React from 'react';
import { StyleSheet } from 'react-native';
import AnimatedLoader from 'react-native-animated-loader';
import PropTypes from 'prop-types';

const sourceLoader = require('../assets/loader.json');

export default function Loader({ visible }) {
  return (
    <AnimatedLoader
      visible={visible}
      overlayColor="rgba(255,255,255,0.75)"
      source={sourceLoader}
      animationStyle={styles.lottie}
      speed={1}
    />
  );
}

Loader.propTypes = {
  visible: PropTypes.bool.isRequired,
};

const styles = StyleSheet.create({
  lottie: {
    width: 200,
    height: 200,
  },
});

/**
 * Ce fichier permet d'utiliser la caméra lorsque la réalité augmentée n'est pas disponible
 */
import React, {
  useState, useRef, useContext,
} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import Tts from 'react-native-tts';
import uuid from 'uuid';
import PropTypes from 'prop-types';

import { AppContext } from '../util/Store';
import Layout from '../util/Layout';
import Multilingue from '../util/Multilingue';
import FetchVision from '../util/FetchVision';
import VoiceRecognition from '../util/VoiceRecognition';
import DialogFlow from '../api/DialogFlow';
import ImageClassifictionResults from './ImageClassificationResults';
import FullScreenBackgroundImage from './FullScreenBackgroundImage';
import ImageLocalisationResults from './ImageLocalisationResults';
import GoogleAutoML from '../api/GoogleAutoML';

export default function CameraScreen({ navigation }) {
  const [photo, setPhoto] = useState(null);
  const [resultsObjectDetection, setResultsObjectDetection] = useState([]);
  const [classificationResults, setClassificationResults] = useState([]);
  const [actualMode, setActualMode] = useState(0);

  /**
   * Références sur la caméra et la scrollview
   */
  const camera = useRef(null);
  const modesScrollView = useRef(null);

  /**
 * Utilisation du contexte
 */
  const appContext = useContext(AppContext);
  const {
    tokenAutoml, setLoading, language, tokenDialogflow, modelName,
  } = appContext;

  Tts.getInitStatus().then(() => {
    Tts.setDefaultLanguage(language);
  }, (err) => {
    if (err.code === 'no_engine') {
      Tts.requestInstallEngine();
    }
  });

  /**
   * On commence la reconnaissance vocale
   */
  const beginVoiceRecognition = async () => {
    VoiceRecognition.startRecognizing(language, onVoiceResults, onVoiceError);
  };

  /**
   * Résultats de la reconnaissance vocale
   */
  const onVoiceResults = async (voiceResult) => {
    try {
      const dialogflowResults = await DialogFlow.detectIntent(tokenDialogflow, voiceResult[0], uuid.v4(), language);

      if (dialogflowResults) {
        const { queryResult } = dialogflowResults;

        const textResult = queryResult.fulfillmentText;

        if (textResult === Multilingue.translateText('Localization')) {
          Tts.speak(Multilingue.translateText('analyseInProgress'));
          Tts.speak(`Mode ${textResult}`);
          await fetchGoogleForLocalization();
        } else if (textResult === Multilingue.translateText('Classification')) {
          Tts.speak(Multilingue.translateText('analyseInProgress'));
          Tts.speak(`Mode ${textResult}`);
          await fetchGoogleForClassification();
        } else if (textResult === Multilingue.translateText('Personalization')) {
          if (modelName) {
            Tts.speak(Multilingue.translateText('analyseInProgress'));
            Tts.speak(`Mode ${textResult}`);
            await fetchGoogleForPersonalization();
          } else {
            Tts.speak(Multilingue.translateText('noModelTrainedSpeech'));
          }
        } else if (textResult === Multilingue.translateText('Gallery')) {
          Tts.speak(Multilingue.translateText('analyseInProgress'));
          Tts.speak(`Mode ${textResult}`);
          navigation.navigate('ImagePicker');
        } else {
          Tts.speak(textResult);
        }
      } else {
        Tts.speak(Multilingue.translateText('dialogflowError'));
      }
    } catch (err) {
      console.log(err);
    }

    setLoading(false);
  };

  /**
   * Si on a une error lors de la reconnaissance vocale
   */
  const onVoiceError = () => {
    setLoading(false);
  };

  /**
   * Utilisation du mode de localisation
   */
  const fetchGoogleForLocalization = async () => {
    const image = await takePicture();
    if (image) {
      try {
        const results = await FetchVision.fetchGoogleVisionAndTranslateMode(0, image);
        if (results.length === 0) {
          Tts.speak(Multilingue.translateText('noneDetectedObject'));
        } else {
          setResultsObjectDetection(results);
          results.forEach((objectDetected) => {
            const { name, score } = objectDetected;
            Tts.speak(`${name}, ${score} ${Multilingue.translateText('percent')}`);
          });
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  /**
   * Uilisation du mode de classification
   */
  const fetchGoogleForClassification = async () => {
    const image = await takePicture();

    if (image) {
      try {
        const results = await FetchVision.fetchGoogleVisionAndTranslateMode(1, image);
        if (results.length === 0) {
          Tts.speak(Multilingue.translateText('noneDetectedObject'));
        } else {
          setClassificationResults(results);
          results.forEach((objectDetected) => {
            const { name, score } = objectDetected;

            Tts.speak(`${name}, ${score} ${Multilingue.translateText('percent')}`);
          });
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  /**
   * Utilisation du mode de personnalisation
   */
  const fetchGoogleForPersonalization = async () => {
    const image = await takePicture();
    if (image) {
      try {
        const results = await GoogleAutoML.imagePrediction(tokenAutoml, modelName, image);
        setClassificationResults(results);
        if (results.length === 0) {
          Tts.speak(Multilingue.translateText('noneDetectedObject'));
        } else {
          setClassificationResults(results);
          results.forEach((objectDetected) => {
            const nameToSpeech = objectDetected.name.replace(/_/g, ' ');
            const { score } = objectDetected;

            Tts.speak(`${nameToSpeech}, ${score} ${Multilingue.translateText('percent')}`);
          });
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  /**
   * Prend une photographie
   */
  const takePicture = async () => {
    const options = {
      quality: 0.5,
      base64: true,
      doNotSave: true,
      forceUpOrientation: true,
      fixOrientation: true,
      orientation: 'portrait',
    };
    const image = await camera.current.takePictureAsync(options);
    if (image.base64) {
      setPhoto(image.base64);
      return image.base64;
    }
    return null;
  };

  /**
   * Permet de faire défiler les menus
   * @param {*} event événement du défilement
   */
  const onScroll = (event) => {
    const xOffset = event.nativeEvent.contentOffset.x;
    const { width } = Layout.window;
    const swipedMode = xOffset / width;
    if ((xOffset % width) === 0 && swipedMode !== actualMode) {
      if (swipedMode === 0) {
        Tts.speak(`Mode ${Multilingue.translateText('Localization')}`);
      } else if (swipedMode === 1) {
        Tts.speak(`Mode ${Multilingue.translateText('Classification')}`);
      } else if (swipedMode === 2) {
        Tts.speak(`Mode ${Multilingue.translateText('Personalization')}`);
      }
      setActualMode(swipedMode);
    }
  };

  /**
   * Déclenche la reconnaissace vocale
   */
  const onPress = () => {
    setLoading(true);
    setClassificationResults([]);

    beginVoiceRecognition();
  };

  /**
   * Active le mode sélectionné par le swipe
   */
  const activeMode = async (mode) => {
    Tts.stop();
    setLoading(true);
    setResultsObjectDetection([]);
    setClassificationResults([]);
    Tts.speak(Multilingue.translateText('analyseInProgress'));
    if (mode === 0) {
      Tts.speak(`Mode ${Multilingue.translateText('Localization')}`);
      await fetchGoogleForLocalization();
    } else if (mode === 1) {
      Tts.speak(`Mode ${Multilingue.translateText('Classification')}`);
      await fetchGoogleForClassification();
    } else if (mode === 2) {
      Tts.speak(`Mode ${Multilingue.translateText('Personalization')}`);
      await fetchGoogleForPersonalization();
    }
    setLoading(false);
  };

  /**
   * Action lors d'un press long
   */
  const onLongPress = (mode) => {
    activeMode(mode);
  };

  /**
 * Suppression des résultats
 */
  const deleteResults = () => {
    setPhoto(null);
    setClassificationResults([]);
    setResultsObjectDetection([]);
  };

  return (
    <View
      style={styles.container}
    >
      {photo ? (
        <TouchableOpacity
          style={styles.container}
          onPress={deleteResults}
        >
          <>
            <FullScreenBackgroundImage imagePicked={{ mime: 'image/jpeg', data: photo }} />
            <ImageLocalisationResults resultsObjectDetection={resultsObjectDetection} />
            <ImageClassifictionResults classificationResults={classificationResults} />
          </>
        </TouchableOpacity>
      )
        : (

          <RNCamera
            style={styles.preview}
            ref={camera}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.auto}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera', // TODO
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
          >
            <ScrollView
              pagingEnabled
              ref={modesScrollView}
              style={styles.modeScrollView}
              contentContainerStyle={styles.ScrollViewContentContainer}
              horizontal
              onScroll={onScroll}
            >
              <TouchableOpacity
                onPress={() => onPress()}
                onLongPress={() => onLongPress(0)}
                style={[styles.menuView, styles.modeView]}
              >
                <Text>{actualMode}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => onPress()}
                onLongPress={() => onLongPress(1)}
                style={[styles.menuView, styles.modeView]}
              >
                <Text>{actualMode}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => onPress()}
                onLongPress={() => onLongPress(2)}
                style={[styles.menuView, styles.modeView]}
              >
                <Text>{actualMode}</Text>
              </TouchableOpacity>
            </ScrollView>
          </RNCamera>
        )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ScrollViewContentContainer: {
    flexGrow: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  viewNoCamera: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  viewButtons: {
    position: 'absolute',
    top: 20,
    flex: 1,
    padding: 10,
    flexDirection: 'row',
  },
  settings: {
    position: 'absolute',
    top: '5%',
    right: '1%',
    backgroundColor: '#92BBD9',
  },
  viewSwiperItem: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  touchableSwiperItem: {
    flex: 1,
    width: Layout.window.width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSwiperItem: {
    fontSize: 18,
    marginBottom: 10,
    color: 'white',
  },
  menuView: {
    flex: 1,
  },
  modeView: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    width: Layout.window.width,
  },
});


CameraScreen.propTypes = {
  navigation: PropTypes.shape().isRequired,
};

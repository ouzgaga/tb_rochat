/**
 * Permet d'afficher un cercle de pourcentage
 */
import React from 'react';
import PercentageCircle from 'react-native-percentage-circle';
import PropTypes from 'prop-types';

export default function PercentageCircleComponent({ percent }) {
  return (
    <PercentageCircle
      borderWidth={10}
      radius={70}
      percent={percent}
      color="#4B77BE"
      textStyle={{ fontSize: 24 }}
    />
  );
}

PercentageCircleComponent.propTypes = {
  percent: PropTypes.string.isRequired,
};

/**
 * Permet d'afficher les résultats pour le mode classification
 */
import React from 'react';
import {
  Text,
  StyleSheet,
  View,
} from 'react-native';
import PropTypes from 'prop-types';

export default function ImageClassificationResults({ classificationResults }) {
  return (
    <View style={styles.classificationResultsView}>
      {classificationResults.map(result => (
        <View key={result.name} style={styles.labelView}>
          <Text style={styles.labelText}>{result.name}</Text>
          <Text style={styles.labelText}>{`${result.score}%`}</Text>
        </View>
      ))}
    </View>
  );
}

ImageClassificationResults.propTypes = {
  classificationResults: PropTypes.arrayOf(PropTypes.shape().isRequired).isRequired,
};

const styles = StyleSheet.create({
  classificationResultsView: {
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
  },
  labelView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  labelText: {
    color: 'white',
    textShadowColor: 'black',
    fontSize: 18,
    textShadowRadius: 2,
    fontWeight: 'bold',
  },
});

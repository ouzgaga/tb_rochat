/**
 * Permet d'afficher le logo de l'application
 */
import React from 'react';
import {
  Image,
} from 'react-native';
import PropTypes from 'prop-types';

const logo = require('../assets/logo.png');

export default function Logo({ width, height }) {
  return (
    <Image
      source={logo}
      style={{ width, height }}
    />
  );
}

Logo.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
};

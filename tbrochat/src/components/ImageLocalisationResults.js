/**
 * Permet l'affichage des résultats de la localisation sans utiliser la réalité augmentée
 */
import React from 'react';
import {
  Text,
  StyleSheet,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import Svg, { Rect } from 'react-native-svg';

import RectanglesColor from '../util/RectangleColor';

export default function ImageResults({ resultsObjectDetection }) {
  return (
    <View pointerEvents="none" style={styles.container}>

      <Svg height="100%" width="100%">
        {resultsObjectDetection.map(({ vertices }, index) => (

          <Rect
            key={RectanglesColor[index].name}
            x={`${vertices.x1 * 100}%`}
            y={`${vertices.y1 * 100}%`}
            width={`${(vertices.x2 - vertices.x1) * 100}%`}
            height={`${(vertices.y2 - vertices.y1) * 100}%`}
            stroke={RectanglesColor[index].color}
            strokeWidth="2"
            fill="none"
          />
        ))}
      </Svg>

      {resultsObjectDetection.map((currElement, index) => (
        <Text
          key={RectanglesColor[index].name}
          style={{
            position: 'absolute',
            top: `${currElement.vertices.y1 * 100 - 3}%`,
            left: `${currElement.vertices.x1 * 100}%`,
            color: RectanglesColor[index].color,
            width: `${(currElement.vertices.x2 - currElement.vertices.x1) * 100}%`,
          }}
        >
          {`${currElement.name} ${currElement.score}%`}
        </Text>
      ))}
    </View>
  );
}

ImageResults.propTypes = {
  resultsObjectDetection: PropTypes.arrayOf(PropTypes.shape().isRequired).isRequired,
};

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
});

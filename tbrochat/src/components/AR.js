/**
 * Permet d'avoir la scène de réalité augmentée
 */
import React, {
  useState, useRef, useContext,
} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  PixelRatio,
} from 'react-native';
import PropTypes from 'prop-types';

import {
  ViroARScene,
  ViroText,
  ViroConstants,
  ViroARSceneNavigator,

} from 'react-viro';

import Tts from 'react-native-tts';
import uuid from 'uuid';
import APIKeys from '../constants/APIKeys';

import { AppContext } from '../util/Store';
import Layout from '../util/Layout';
import Multilingue from '../util/Multilingue';
import FetchVision from '../util/FetchVision';
import VoiceRecognition from '../util/VoiceRecognition';
import DialogFlow from '../api/DialogFlow';
import ThreeDimensions from '../util/ThreeDimensions';
import GoogleAutoML from '../api/GoogleAutoML';
import ImageClassifictionResults from './ImageClassificationResults';

/**
 * Pour la conversion de l'image en base 64
 */
const RNFS = require('react-native-fs');

/**
 * Afin de jouer différents sons
 */
const Sound = require('react-native-sound');

Sound.setCategory('Playback');

/**
 * Les différents sons joués
 */
const sound1 = new Sound('beep1.wav', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log(error);
  }
});

const sound2 = new Sound('beep2.wav', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log(error);
  }
});

const sound3 = new Sound('beep3.wav', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log(error);
  }
});

const { height, width } = Layout.window;

export default function AR({ navigation, setTrackingInterval, setPositionInterval }) {
  const [initializing, setInitialing] = useState(true);
  const [trackingObject, setTrackingObject] = useState(null);
  const [text, setText] = useState('');
  const [actualMode, setActualMode] = useState(0);
  const [actualOrientation, setActualOrientation] = useState(null);
  const [resultsObjectDetection, setResultsObjectDetection] = useState([]);
  const [classificationResults, setClassificationResults] = useState([]);

  const modesScrollView = useRef(null);
  const arscene = useRef(null);
  const sceneNavigator = useRef(null);

  const appContext = useContext(AppContext);
  const {
    tokenAutoml, setLoading, language, tokenDialogflow, modelName,
  } = appContext;

  /**
   * Démarre le text to speech
   */
  Tts.getInitStatus().then(() => {
    Tts.setDefaultLanguage(language);
  }, (err) => {
    if (err.code === 'no_engine') {
      Tts.requestInstallEngine();
    }
  });

  /**
   * Changement de l'état du tracking
   */
  const onTrackingUpdated = (state, reason) => {
    if (state === ViroConstants.TRACKING_NORMAL) {
      setInitialing(false);
    } else {
      // Tracking de la position de la caméra dans le monde inconnu (TRACKING_UNAVAILABLE) ou incertain(TRACKING_LIMITED)
      // On remet les résultats à 0
      setInitialing(true);
      setResultsObjectDetection([]);
      setClassificationResults([]);
      setTrackingObject(null);
      setText('');
      setLoading(false);
      setTrackingInterval(0);
      setPositionInterval(0);
    }
  };

  /**
   * Début de la reconnaissance vocale
   */
  const beginVoiceRecognition = async () => {
    VoiceRecognition.startRecognizing(language, onVoiceResults, onVoiceError);
  };

  /**
   * Lorsque l'on reçoit des résultats à la reconnaissance vocale
   * @param {*} voiceResult résultat de la reconnaissance vocale
   */
  const onVoiceResults = async (voiceResult) => {
    setLoading(true);

    if (resultsObjectDetection.length) {
      for (let i = 0; i < resultsObjectDetection.length; i += 1) {
        for (let j = 0; j < voiceResult.length; j += 1) {
          if (voiceResult[j].toLowerCase().includes(resultsObjectDetection[i].name.toLowerCase())) {
            setTrackingObject(resultsObjectDetection[i]);
            beginTrackingObject(resultsObjectDetection[i]);
            setLoading(false);

            return;
          }
        }
      }
    }

    try {
      const dialogflowResults = await DialogFlow.detectIntent(tokenDialogflow, voiceResult[0], uuid.v4(), language);

      if (dialogflowResults) {
        const { queryResult } = dialogflowResults;

        const textResult = queryResult.fulfillmentText;

        if (textResult === Multilingue.translateText('Localization')) {
          Tts.speak(Multilingue.translateText('analyseInProgress'));
          Tts.speak(`Mode ${textResult}`);
          await fetchGoogleForTracking();
          setPositionInterval(setInterval(getActualOrientation, 200));
        } else if (textResult === Multilingue.translateText('Classification')) {
          Tts.speak(Multilingue.translateText('analyseInProgress'));
          Tts.speak(`Mode ${textResult}`);
          await fetchGoogleForScene();
        } else if (textResult === Multilingue.translateText('Personalization')) {
          if (modelName) {
            Tts.speak(Multilingue.translateText('analyseInProgress'));
            Tts.speak(`Mode ${textResult}`);
            await personnalizationMode();
          } else {
            Tts.speak(Multilingue.translateText('noModelTrainedSpeech'));
          }
        } else if (textResult === Multilingue.translateText('Gallery')) {
          Tts.speak(Multilingue.translateText('analyseInProgress'));
          Tts.speak(`Mode ${textResult}`);
          navigation.navigate('ImagePicker');
        } else {
          Tts.speak(textResult);
        }
      } else {
        Tts.speak(Multilingue.translateText('dialogflowError'));
      }
    } catch (err) {
      console.log(err);
    }

    setLoading(false);
  };

  /**
   * Mode localisation
   */
  const fetchGoogleForTracking = async () => {
    try {
      const orientation = await arscene.current.getCameraOrientationAsync();
      const imageToSend = await sceneNavigator.current.sceneNavigator.takeScreenshot('viroPhoto', false);
      if (imageToSend.success) {
        // le screenshot a bien réussi
        const base64image = await RNFS.readFile(imageToSend.url, 'base64');

        const results = await FetchVision.fetchGoogleVisionAndTranslateMode(0, base64image);

        const resultsArray = [];

        await Promise.all(results.map(async (item) => {
          const { vertices, name, score } = item;
          const {
            x1, x2, y1, y2,
          } = vertices;

          const array = [];
          const interval = 5;
          const deltaX = ((x2 - x1) / interval) - 0.001;
          const deltaY = ((y2 - y1) / interval) - 0.001;

          for (let x = x1 + deltaX; x <= x2 - deltaX; x += (x2 - x1) / interval) {
            for (let y = y1 + deltaY; y <= y2 - deltaY; y += (y2 - y1) / interval) {
              const resultsARHitTesting = await arscene.current.performARHitTestWithPoint(Math.round(width * x * PixelRatio.get()), Math.round(height * y * PixelRatio.get()));

              if (resultsARHitTesting.length > 0) {
                const distance = ThreeDimensions.distanceBetweenTwoPoints(orientation.position, resultsARHitTesting[0].transform.position);
                array.push({ distance, position: resultsARHitTesting[0].transform.position }); // todo : faire pour tous les results
              }
            }
          }

          if (array.length > 0) {
            let meanPositionX = 0;
            let meanPositionY = 0;
            let meanPositionZ = 0;
            let meanDistance = 0;
            for (let index = 0; index < array.length; index += 1) {
              meanPositionX += array[index].position[0];
              meanPositionY += array[index].position[1];
              meanPositionZ += array[index].position[2];
              meanDistance += array[index].distance;
            }

            meanPositionX /= array.length;
            meanPositionY /= array.length;
            meanPositionZ /= array.length;
            meanDistance /= array.length;

            const newObject = {
              name,
              vertices,
              score,
              position: [meanPositionX, meanPositionY, meanPositionZ],
              distance: meanDistance,
            };
            resultsArray.push(newObject);
          } else {
            const newObject = {
              name,
              vertices,
              score,
              position: null,
              distance: null,
            };
            resultsArray.push(newObject);
          }
        }));

        setResultsObjectDetection(resultsArray);

        const nearObjects = resultsArray.filter(item => item.distance && item.distance <= 1);
        if (nearObjects.length) {
          Tts.speak(Multilingue.translateText('nearObjects'));
          nearObjects.forEach((objectDetected) => {
            Tts.speak(objectDetected.name);
          });
        }

        const farObjects = resultsArray.filter(item => item.distance && item.distance > 1);
        if (farObjects.length) {
          Tts.speak(Multilingue.translateText('farObjects'));
          farObjects.forEach((objectDetected) => {
            Tts.speak(objectDetected.name);
          });
        }

        const noDistanceObjects = resultsArray.filter(item => item.distance === null);
        if (noDistanceObjects.length) {
          Tts.speak(Multilingue.translateText('noDistanceObjects'));
          noDistanceObjects.forEach((objectDetected) => {
            Tts.speak(objectDetected.name);
          });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Mode classification
   */
  const fetchGoogleForScene = async () => {
    try {
      const imageToSend = await sceneNavigator.current.sceneNavigator.takeScreenshot('viroPhoto', false);
      if (imageToSend.success) {
        // le screenshot a bien réussi
        const base64image = await RNFS.readFile(imageToSend.url, 'base64');

        const results = await FetchVision.fetchGoogleVisionAndTranslateMode(1, base64image);

        setClassificationResults(results);
        results.forEach((objectDetected) => {
          const { name, score } = objectDetected;

          Tts.speak(`${name}, ${score} ${Multilingue.translateText('percent')}`);
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Mode personnalisation
   */
  const personnalizationMode = async () => {
    try {
      const imageToSend = await sceneNavigator.current.sceneNavigator.takeScreenshot('viroPhoto', false);
      if (imageToSend.success) {
        // le screenshot a bien réussi

        const base64image = await RNFS.readFile(imageToSend.url, 'base64');

        const results = await GoogleAutoML.imagePrediction(tokenAutoml, modelName, base64image);

        if (results.length === 0) {
          Tts.speak(Multilingue.translateText('noneDetectedObject'));
        } else {
          setClassificationResults(results);
          results.forEach((objectDetected) => {
            const nameToSpeech = objectDetected.name.replace(/_/g, ' ');
            const { score } = objectDetected;

            Tts.speak(`${nameToSpeech}, ${score} ${Multilingue.translateText('percent')}`);
          });
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * Lorsque la reconnaissance vocale transmet une erreur
   */
  const onVoiceError = () => {
    setLoading(false);
  };

  /**
   * Permet de changer de mode
   * @param {*} event l'évenement de changement de mode
   */
  const onScroll = (event) => {
    const xOffset = event.nativeEvent.contentOffset.x;
    const swipedMode = xOffset / width;
    if ((xOffset % width) === 0 && swipedMode !== actualMode) {
      if (swipedMode === 0) {
        Tts.speak(`Mode ${Multilingue.translateText('Localization')}`);
      } else if (swipedMode === 1) {
        Tts.speak(`Mode ${Multilingue.translateText('Classification')}`);
      } else if (swipedMode === 2) {
        Tts.speak(`Mode ${Multilingue.translateText('Personalization')}`);
      }
      setActualMode(swipedMode);
    }
  };

  /**
   * l'utilisateur a appuyé sur l'écran
   */
  const onPress = () => {
    Tts.stop();
    if (initializing) {
      Tts.speak(Multilingue.translateText('initializingInProgress'));
    } else {
      setClassificationResults([]);
      setText('');
      setLoading(true);
      beginVoiceRecognition();
      if (trackingObject) {
        setTrackingObject(null);
        setTrackingInterval(0);
      }
    }
  };

  /**
   * Activation du mode sélectionné
   * @param {*} mode le mode sélectionné
   */
  const activeMode = async (mode) => {
    Tts.stop();
    setLoading(true);
    setTrackingObject(null);
    setResultsObjectDetection([]);
    setClassificationResults([]);
    setText('');
    setTrackingInterval(0);
    Tts.speak(Multilingue.translateText('analyseInProgress'));
    if (mode === 0) {
      Tts.speak(`Mode ${Multilingue.translateText('Localization')}`);
      await fetchGoogleForTracking();
      setPositionInterval(setInterval(getActualOrientation, 200));
    } else if (mode === 1) {
      Tts.speak(`Mode ${Multilingue.translateText('Classification')}`);
      await fetchGoogleForScene();
    } else if (mode === 2) {
      Tts.speak(`Mode ${Multilingue.translateText('Personalization')}`);
      await personnalizationMode();
    }
    setLoading(false);
  };

  /**
   * L'utilisateur a appuyé longtemps sur l'écran
   * @param {*} mode le mode actuel
   */
  const onLongPress = (mode) => {
    if (initializing) {
      Tts.speak(Multilingue.translateText('initializingInProgress'));
    } else {
      activeMode(mode);
    }
  };

  /**
   * Retourne l'orientation actuelle du smartphone
   */
  const getActualOrientation = async () => {
    setActualOrientation(await arscene.current.getCameraOrientationAsync());
  };

  /**
   * Commencement du tracking d'un objet
   * @param {*} objectToTrack l'objet à tracker
   */
  const beginTrackingObject = (objectToTrack) => {
    setTrackingInterval(setInterval(() => calculeAngle(objectToTrack), 300));
  };

  /**
   * Calcul de l'angle par rapport à l'axe de la caméra et émission d'un son en conséquence
   * @param {*} objectToTrack 
   */
  const calculeAngle = async (objectToTrack) => {
    const orientation = await arscene.current.getCameraOrientationAsync();
    setActualOrientation(orientation);

    if (objectToTrack) {
      const distanceWithObject = ThreeDimensions.distanceBetweenTwoPoints(orientation.position, objectToTrack.position);
      let soundVolume = Math.exp(-distanceWithObject / 2);
      if (soundVolume < 0.2) {
        soundVolume = 0.2;
      }

      const vecteur = [objectToTrack.position[0] - orientation.position[0], objectToTrack.position[1] - orientation.position[1], objectToTrack.position[2] - orientation.position[2]];
      const angleHorizontal = ThreeDimensions.determinantHorizontal(orientation.forward, vecteur);
      if (angleHorizontal >= -0.2 && angleHorizontal <= 0.2) {
        const angleVertical = ThreeDimensions.determinantVertical(orientation.forward, vecteur);
        if (angleVertical >= -0.2 && angleVertical <= 0.2) {
          setText(Multilingue.translateText('forward'));

          sound1.setVolume(soundVolume);

          sound1.play((success) => {
            if (!success) {
              console.log('Sound did not play');
            }
          });
        } else if (angleVertical > 0.2) {
          sound2.setVolume(soundVolume);

          sound2.play((success) => {
            if (!success) {
              console.log('Sound did not play');
            }
          });
          setText(Multilingue.translateText('up'));
        } else {
          sound3.setVolume(soundVolume);

          sound3.play((success) => {
            if (!success) {
              console.log('Sound did not play');
            }
          });
          setText(Multilingue.translateText('bottom'));
        }
      } else if (angleHorizontal > 0.1) {
        setText(Multilingue.translateText('right'));
      } else {
        setText(Multilingue.translateText('left'));
      }
    }
  };

  /**
   * Notre scène de réalité augmentée
   * @param {*} param0 les props reçus en paramètre
   */
  const ARScene = ({ arSceneNavigator }) => (

    <ViroARScene
      ref={arscene}
      onTrackingUpdated={onTrackingUpdated}
      displayPointCloud
    >

      {!arSceneNavigator.viroAppProps.initializing && (
        <>
          {
            arSceneNavigator.viroAppProps.resultsObjectDetection.map(result => result.position && (
              <ViroText
                key={result.name}
                text={result.name}
                textAlign="left"
                textAlignVertical="top"
                color="#ff0000"
                scale={[0.2, 0.2, 0.2]}
                style={styles.initializingTextStyle}
                position={result.position}
                rotation={arSceneNavigator.viroAppProps.actualOrientation && arSceneNavigator.viroAppProps.actualOrientation.rotation}
              />
            ))
          }
          {
            arSceneNavigator.viroAppProps.trackingObject && (
              <ViroText
                text={arSceneNavigator.viroAppProps.text}
                scale={[0.5, 0.5, 0.5]}
                position={arSceneNavigator.viroAppProps.trackingObject.position}
                style={styles.initializingTextStyle}
                rotation={arSceneNavigator.viroAppProps.actualOrientation && arSceneNavigator.viroAppProps.actualOrientation.rotation}
              />
            )
          }
        </>
      )
      }
    </ViroARScene>
  );

  ARScene.propTypes = {
    arSceneNavigator: PropTypes.shape().isRequired,
  };

  return (
    <View
      style={styles.container}
    >
      <ViroARSceneNavigator
        apiKey={APIKeys.API_VIROREACT}
        ref={sceneNavigator}
        viroAppProps={{
          initializing,
          resultsObjectDetection,
          classificationResults,
          trackingObject,
          actualOrientation,
          text,
        }}
        initialScene={{ scene: ARScene }}
      />

      {initializing && (
        <View style={styles.initializingView}>
          <Text style={styles.initializingTextStyle}>{Multilingue.translateText('initialitingAR')}</Text>
        </View>
      )}

      <ScrollView
        pagingEnabled
        ref={modesScrollView}
        style={styles.modeScrollView}
        contentContainerStyle={styles.ScrollViewContentContainer}
        horizontal
        onScroll={onScroll}
      >
        <TouchableOpacity
          onPress={() => onPress()}
          onLongPress={() => onLongPress(0)}
          delayLongPress={2000}
          style={[styles.menuView, styles.modeView]}
        />
        <TouchableOpacity
          onPress={() => onPress()}
          onLongPress={() => onLongPress(1)}
          delayLongPress={2000}
          style={[styles.menuView, styles.modeView]}
        />
        {modelName && (
          <TouchableOpacity
            onPress={() => onPress()}
            onLongPress={() => onLongPress(2)}
            delayLongPress={2000}
            style={[styles.menuView, styles.modeView]}
          />
        )}

      </ScrollView>
      <ImageClassifictionResults classificationResults={classificationResults} />
    </View>
  );
}

AR.propTypes = {
  navigation: PropTypes.shape().isRequired,
  setTrackingInterval: PropTypes.func.isRequired,
  setPositionInterval: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ScrollViewContentContainer: {
    flexGrow: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  viewNoCamera: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  modeScrollView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  viewButtons: {
    position: 'absolute',
    top: 20,
    flex: 1,
    padding: 10,
    flexDirection: 'row',
  },
  settings: {
    position: 'absolute',
    top: '5%',
    right: '1%',
    backgroundColor: '#92BBD9',
  },
  viewSwiperItem: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  touchableSwiperItem: {
    flex: 1,
    width: Layout.window.width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSwiperItem: {
    fontSize: 18,
    marginBottom: 10,
    color: 'white',
  },
  menuView: {
    flex: 1,
  },
  modeView: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    width: Layout.window.width,
  },
  initializingTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  initializingView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
});

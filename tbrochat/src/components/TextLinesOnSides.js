
import React from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

export default function TextLinesOnSides({ textSize, title }) {
  return (

    <View style={styles.container}>
      <View style={styles.firstLine} />
      <Text style={[styles.text, { fontSize: textSize }]}>{title}</Text>
      <View style={styles.secondLine} />
    </View>
  );
}

TextLinesOnSides.propTypes = {
  textSize: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 20,
  },
  firstLine: {
    backgroundColor: 'black',
    height: 0.5,
    flex: 1,
    alignSelf: 'center',
  },
  text: {
    alignSelf: 'center',
    paddingHorizontal: 5,
    fontWeight: 'bold',
  },
  secondLine: {
    backgroundColor: 'black',
    height: 0.5,
    flex: 1,
    alignSelf: 'center',
  },
});

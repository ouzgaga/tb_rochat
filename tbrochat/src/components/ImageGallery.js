/**
 * Permet d'afficher la galerie d'iamges
 */
import React, { useState, useContext } from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  Button, Text, Icon, Picker,
} from 'native-base';
import FastImage from 'react-native-fast-image';

import Multilingue from '../util/Multilingue';
import Layout from '../util/Layout';
import Firebase from '../api/Firebase';
import { AppContext } from '../util/Store';

export default function ImageGallery({ images, labelSelected }) {
  const [deleteMode, setDeleteMode] = useState(false);
  const [selectedLabel, setSelectedLabel] = useState(labelSelected.labelId);
  const [selectedImagesForDelete, setSelectedImagesForDelete] = useState([]);

  const appContext = useContext(AppContext);
  const { userLabels } = appContext;

  /**
   * Lorsque l'on presse sur une image
   * @param {*} imagePressed l'image que l'on a pressée
   */
  const onPressOnImage = (imagePressed) => {
    if (deleteMode) {
      if (selectedImagesForDelete.includes(imagePressed)) {
        setSelectedImagesForDelete(selectedImagesForDelete.filter(image => image !== imagePressed));
      } else {
        setSelectedImagesForDelete([...selectedImagesForDelete, imagePressed]);
      }
    } else {
      Firebase.changeLabelForImage(imagePressed, selectedLabel);
    }
  };

  /**
   * Lorsque l'on reste appuyer sur une image
   * @param {*} newImageforDeletion l'image sur laquelle on est resté appuyé
   */
  const onLongPressOnImage = (newImageforDeletion) => {
    if (deleteMode === false) {
      setDeleteMode(true);
      setSelectedImagesForDelete([...selectedImagesForDelete, newImageforDeletion]);
    }
  };

  /**
   * Permet d'annuler la suppression des images
   */
  const cancelDeletion = () => {
    setDeleteMode(false);
    setSelectedImagesForDelete([]);
  };

  /**
   * Permet de supprimer les images sélectionnées
   */
  const deleteImages = () => {
    Alert.alert(
      '',
      `${Multilingue.translateText('delete')} ${selectedImagesForDelete.length} ${selectedImagesForDelete.length > 1 ? Multilingue.translateText('images') : Multilingue.translateText('image')}`,
      [
        {
          text: `${Multilingue.translateText('cancel')}`,
          onPress: () => { },
          style: 'cancel',
        },
        {
          text: `${Multilingue.translateText('delete')}`,
          onPress: () => {
            selectedImagesForDelete.forEach((imageId) => {
              Firebase.deleteImage(imageId);
            });
            cancelDeletion();
          },
        },
      ],
      { cancelable: true },
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {images.length ? (
        <>
          <View style={styles.deleteButtons}>
            {deleteMode ? (
              <>
                <Button transparent disabled>
                  <Text>{selectedImagesForDelete.length}</Text>
                </Button>
                {selectedImagesForDelete.length > 0 && (
                  <Button transparent onPress={deleteImages}>
                    <Text>{Multilingue.translateText('delete')}</Text>
                  </Button>
                )}
                <Button transparent onPress={cancelDeletion}>
                  <Text>{Multilingue.translateText('cancel')}</Text>
                </Button>
              </>
            ) : (
                <>
                  <Text>{Multilingue.translateText('label')}</Text>

                  <Picker
                    note
                    mode="dropdown"
                    style={{ width: 120 }}
                    selectedValue={selectedLabel}
                    onValueChange={(value) => { setSelectedLabel(value); }}
                  >
                    {userLabels.map(label => (<Picker.Item key={label.labelId} label={label.data.name} value={label.labelId} />))}

                    <Picker.Item label={Multilingue.translateText('unlabeled')} value={null} />

                  </Picker>
                </>
              )}
          </View>
          <ScrollView>
            <View style={styles.scrollView}>
              {images.map(image => (
                <TouchableOpacity
                  onPress={() => { onPressOnImage(image.imageId); }}
                  onLongPress={() => { onLongPressOnImage(image.imageId); }}
                  key={image.imageId}
                >
                  <View
                    style={styles.imageView}
                  >
                    <FastImage
                      style={styles.image}
                      source={{ uri: image.data.downloadLink }}
                    />
                    <Text style={styles.labelText}>{userLabels.find(label => label.labelId === image.data.label) && userLabels.find(label => label.labelId === image.data.label).data.name}</Text>

                    {deleteMode
                      && (selectedImagesForDelete.includes(image.imageId) ? (
                        <>
                          <Icon
                            style={[styles.buttonCheck, { color: '#3498db' }]}
                            type="MaterialCommunityIcons"
                            name="checkbox-marked-circle"
                          />
                          <View style={styles.opacityView} />
                        </>
                      ) : (
                          <Icon
                            style={styles.buttonCheck}
                            type="MaterialCommunityIcons"
                            name="circle-outline"
                          />
                        )
                      )}

                  </View>
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>
        </>
      ) : (
          <View style={styles.centeredContainer}>
            <Text>{Multilingue.translateText('noImagesToShow')}</Text>
          </View>
        )}
    </SafeAreaView>
  );
}

ImageGallery.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  labelSelected: PropTypes.shape(),
};

ImageGallery.defaultProps = {
  labelSelected: { labelId: null },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  centeredContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  deleteButtons: {
    paddingHorizontal: 10,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
  },
  scrollView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  imageView: {
    width: Layout.window.width / 2 - 20,
    height: Layout.window.width / 2 - 20,
    padding: 3,
    borderWidth: 0.2,
    borderRadius: 3,
    margin: 5,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 3,
  },
  labelText: {
    position: 'absolute',
    bottom: 5,
    left: 5,
    color: 'white',
    textShadowColor: 'black',
    fontSize: 16,
    textShadowRadius: 2,
    fontWeight: 'bold',
  },
  opacityView: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderRadius: 3,
  },
  buttonCheck: {
    position: 'absolute',
    top: 5,
    right: 5,
  },
  iconColor: {
    color: 'white',
  },
});

/**
 * Permet d'afficher une image en fullscreen
 */
import React from 'react';
import {
  Image,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

export default function FullScreenBackgroundImage({ imagePicked }) {
  return (
    <Image
      style={styles.image}
      source={{ uri: `data:${imagePicked.mime};base64,${imagePicked.data}` }}
    />
  );
}

FullScreenBackgroundImage.propTypes = {
  imagePicked: PropTypes.shape().isRequired,
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
  },
});

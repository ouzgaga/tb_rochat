// Traductions en français
const fr = {
  // commun
  error: 'Erreur',
  ok: 'D\'accord',
  yes: 'Oui',
  no: 'Non',
  percent: 'pour cent',

  // Menu Home
  noAccessCamera: "Pas d'accès à la caméra",
  objectsDetectionMenu: "Détection d'objets",
  textDetectionMenu: 'Détection de texte',
  initializingInProgress: 'Initialisation en cours',
  dialogflowError: 'Erreur avec Dialogflow',
  Personalization: 'Personnalisation',
  Classification: 'Classification',
  Localization: 'Localisation',
  Gallery: 'Galerie',
  analyseInProgress: 'Analyse en cours',
  noModelTrainedSpeech: "Vous devez entrainer votre modèle avant d'utiliser le mode personalisation",
  nearObjects: 'Objets proches',
  farObjects: 'Objets lointains',
  noDistanceObjects: 'Objets sans distance',
  noneDetectedObject: 'Aucun objet détecté',

  // Tracking
  initialitingAR: 'Initialisation AR...',
  forward: 'tout droit',
  right: 'droite',
  left: 'gauche',
  bottom: 'bas',
  up: 'haut',
  meters: 'mètres',
  centimeters: 'centimètres',
  details: 'détails',

  // Menu des réglages
  settingsMenu: 'Réglages',
  imageFromGalleryMenu: 'Image de la Galerie',
  exitSettingMenu: 'Quitter les Réglages',
  personalizationLabel: 'Personnalisation',
  newLabelMenu: '+ Nouveau Label',
  newImageMenu: '+ Nouvelle Image',
  imagesLabel: 'Images',
  allImagesMenu: 'Toutes les images',
  labeledImagesMenu: 'Labellisées',
  unlabeledImagesMenu: 'Non Labellisées',
  labelsLabel: 'Labels',
  modelLabel: 'Modèle',
  modelStatisticsMenu: 'Statistiques du modèle',
  trainModelMenu: 'Entrainer le modèle',
  loginMenu: 'Connexion',
  signUpMenu: 'Créer un compte',

  // Ecran Réglages
  language: 'Langue',
  arLabel: 'Réalité Augmentée',
  permissionsLabel: 'Permissions Requises',
  signOut: 'Se déconnecter',
  checkDisconnectUser: 'Etes-vous sûr de vouloir vous déconnecter ?',

  // Image de la galerie menu
  selectImageFromGallery: 'Sélectionner une image de la galerie avec le mode',

  // Menu nouveau label
  labelName: 'Nom du label',
  addLabel: 'Ajouter un nouveau label',
  labelAdded: 'Label ajouté avec succès',

  // Menu nouvelle image
  imageFromGallery: 'Importer des images de la galerie',
  imageFromCamera: 'Prendre une photographie',
  uploadImageInProgress: 'Import des images en cours',
  uploadFinished: 'Importation terminée',

  // Galerie images
  delete: 'Supprimer',
  image: 'image',
  images: 'images',
  cancel: 'Annuler',
  label: 'Label',
  unlabeled: 'Non Labellisées',
  noImagesToShow: 'Aucune image à afficher',

  // Statistiques du modèle
  wholeModel: 'Modèle entier',
  precision: 'Précision',
  recall: 'Rappel',
  noModel: 'Aucun model, entrainez un modèle avant de pouvoir consulter ses statistiques',


  // Entrainement du modèle
  trainModelNotEnoughImages: 'Il faut un minimum de 10 images pour le label:',
  trainModelNotEnoughLabels: 'Il faut au minimum 2 labels',
  trainModelButton: 'Entrainez votre modèle',
  trainModelText: 'Entrainez votre modèle',
  importDatasButton: 'Importez vos images labelisées',
  checkDeleteDataset: 'Etes-vous sûr de vouloir supprimer votre modèle et le dataset ?',
  modelTrained: 'Modèle entrainé !',
  deleteDatasetAndModel: 'Supprimer le dataset et le modèle',
  trainTheModel: 'Entrainer le modèle',
  deleteDataset: 'Supprimer le dataset',
  operationInProgress: 'Opération en cours',
  operationTextDetails: "Cette opération peut prendre plusieurs minutes et dépend du nombre d'images labélisées de votre ensemble de données",
  operationTextDetails2: 'Réessayez de venir sur cette page dans un moment',
  errorDuplicateFiles: 'Images dupliquées détectées, nous vous conseillons de supprimer le dataset et de recommencer',

  // Modification Label
  checkDeleteLabel: 'Supprimer ce label ?',
  alreadyExistingLabel: 'Ce label est déjà existant',
  checkChangeLabelName: 'Modifier ce label ?',
  errorMalformatedLabel: 'Erreur, label malformé',
  malformatedLabelExplain: 'Le label doit commencé par une lettre ou un chiffre et doit contenir uniquement des lettres, des chiffres ou des "_"',
  labelNamePlaceholder: 'Nom du label',

  // Login
  email: 'Email',
  password: 'Mot de passe',
  LogWithEmail: 'Connexion par email',
  logWithFacebook: 'Connexion avec Facebook',
  logWithGoogle: 'Connexion avec Google',
  login: 'Connexion',
  createAccount: 'Créer un compte',
  alreadyAccount: 'Compte déjà existant',
  resetPassword: 'Mot de passe oublié ?',
  logInExistingAccount: 'Connexion avec un compte existant',
  invalidEmail: 'Email invalide',
  userDisabled: 'Utilisateur désactivé',
  userNotFound: 'Utilisateur inexistant',
  wrongPassword: 'Mot de passe erroné',

  // Sign Up
  emailAlreadyInUse: 'Email déjà utilisé',
  operationNotAllowed: 'Opération non autorisée',
  weakPassword: 'Mot de passe faible',
};

export default fr;

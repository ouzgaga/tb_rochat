// Traduciton en anglais
const en = {
  // commun
  error: 'Error',
  ok: 'OK',
  yes: 'Yes',
  no: 'No',
  percent: 'percent',

  // Menu Home
  noAccessCamera: 'No access to camera',
  objectsDetectionMenu: 'Objects Detection',
  textDetectionMenu: 'Text Detection',
  initializingInProgress: 'Initialization in progress',
  dialogflowError: 'Error with Dialogflow',
  Personalization: 'Personalization',
  Classification: 'Classification',
  Localization: 'Localization',
  Gallery: 'Gallery',
  analyseInProgress: 'Analyse in progress',
  noModelTrainedSpeech: 'You must train your own model before using the personalization mode',
  nearObjects: 'Near objects',
  farObjects: 'Far objects',
  noDistanceObjects: 'No distance objects',
  noneDetectedObject: 'No object detected',

  // Tracking
  initialitingAR: 'Initializing AR...',
  forward: 'forward',
  right: 'right',
  left: 'left',
  bottom: 'bottom',
  up: 'up',
  meters: 'meters',
  centimeters: 'centimeters',
  details: 'details',

  // Menu des réglages
  settingsMenu: 'Settings',
  imageFromGalleryMenu: 'Image from Gallery',
  exitSettingMenu: 'Exit Settings',
  personalizationLabel: 'Personalization',
  newLabelMenu: '+ New Label',
  newImageMenu: '+ New Image',
  imagesLabel: 'Images',
  allImagesMenu: 'All Images',
  labeledImagesMenu: 'Labeled',
  unlabeledImagesMenu: 'Unlabeled',
  labelsLabel: 'Labels',
  modelLabel: 'Model',
  modelStatisticsMenu: 'Model Statistics',
  trainModelMenu: 'Train Model',
  loginMenu: 'Login',
  signUpMenu: 'Sign Up',


  // Settings screen
  language: 'Language',
  arLabel: 'Augmented Reality',
  permissionsLabel: 'Permissions Needed',
  signOut: 'Sign Out',
  checkDisconnectUser: 'Do you really want to sign out ?',


  // Image de la galerie menu
  selectImageFromGallery: 'Select image from gallery with mode',

  // Menu nouveau label
  labelName: 'Label Name',
  addLabel: 'Add a new label',
  labelAdded: 'Label Added',

  // Menu nouvelle image
  imageFromGallery: 'Pick Image from Gallery',
  imageFromCamera: 'Take image from camera',
  uploadImageInProgress: 'Upload in progess',
  uploadFinished: 'Upload finished',

  // Galerie images
  delete: 'Delete',
  image: 'image',
  images: 'images',
  cancel: 'Cancel',
  label: 'Label',
  unlabeled: 'Unlabeled',
  noImagesToShow: 'No image to display',

  // Statistiques du modèle
  wholeModel: 'Whole model',
  precision: 'Precision',
  recall: 'Recall',
  noModel: 'No Model, train a model before you can check the stats',

  // Entrainement du modèle
  trainModelNotEnoughImages: 'You need at least 10 images for label :',
  trainModelNotEnoughLabels: 'You need at least 2 differents labels',
  trainModelButton: 'Train your model',
  trainModelText: 'Train your Model',
  importDatasButton: 'Import your labeled images',
  checkDeleteDataset: 'Are you sure you want to delete the dataset and the model ?',
  modelTrained: 'Model trained !',
  deleteDatasetAndModel: 'Delete dataset and model',
  trainTheModel: 'Train dataset',
  deleteDataset: 'Delete dataset',
  operationInProgress: 'Operation in progress',
  operationTextDetails: 'This may take several minutes and depends on the number of labeled images in your dataset',
  operationTextDetails2: 'Try to come back to this page in a moment',
  errorDuplicateFiles: 'Duplicate Files detected, we suggest you to delete the dataset and retry',

  // Modification Label
  checkDeleteLabel: 'Delete this label ?',
  alreadyExistingLabel: 'This label is already existing',
  checkChangeLabelName: 'Modify this label ?',
  errorMalformatedLabel: 'Error, malformed label',
  malformatedLabelExplain: 'The label must begin with a letter or number and must contain only letters, numbers or "_"',
  labelNamePlaceholder: 'Label Name',

  // Login
  email: 'Email',
  password: 'Password',
  LogWithEmail: 'Log With Email',
  logWithFacebook: 'Log With Facebook',
  logWithGoogle: 'Log With Google',
  login: 'Log In',
  createAccount: 'Create an account',
  alreadyAccount: 'Already an Account',
  resetPassword: 'Reset Password ?',
  logInExistingAccount: 'Log In with existing account',
  invalidEmail: 'Invalid email',
  userDisabled: 'User disabled',
  userNotFound: 'User not found',
  wrongPassword: 'Wrong password',

   // Sign Up
   emailAlreadyInUse: 'Email already used',
   operationNotAllowed: 'Operation not allowed',
   weakPassword: 'Weak password',
};

export default en;

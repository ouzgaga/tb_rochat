/**
 * Dimensions de l'écran
 */
import { Dimensions, StatusBar } from 'react-native';

const { width } = Dimensions.get('window');
const { height } = Dimensions.get('window');

export default {
  window: {
    width,
    height,
    statusBarHeight: StatusBar.currentHeight,
  },
  isSmallDevice: width < 375,
};

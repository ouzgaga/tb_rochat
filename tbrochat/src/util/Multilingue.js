/**
 * Classe permettant de gérer différents langages dans l'application
 */
import i18n from 'i18n-js';

import en from './Languages/en';
import fr from './Languages/fr';

i18n.defaultLocale = 'en'; // par défault, la langue est l'anglais si le smartphone n'est ni en 'fr' ni en 'en'

i18n.fallbacks = true; // utilisation de fallbacks si une traduction n'est pas présente dans la langue actuelle
i18n.translations = { fr, en };

const setLocale = (locale) => {
  i18n.locale = locale;
};

const getCurrentLocale = () => i18n.locale;

const translateText = langKey => i18n.t(langKey);

export default { setLocale, getCurrentLocale, translateText };

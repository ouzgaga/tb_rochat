/**
 * Permet d'avoir un état global avec les données utilisées par de nombreux composants
 */
import React, { createContext } from 'react';
import PropTypes from 'prop-types';

export const AppContext = createContext();

export class AppProvider extends React.Component {
  constructor() {
    super();
    this.state = {
      language: 'en',
      setLanguage: this.setLanguage,
      arEnable: false,
      setArEnable: this.setArEnable,
      arSupported: false,
      setArSupported: this.setArSupported,
      allPermissions: false,
      setAllPermissions: this.setAllPermissions,
      loading: false,
      setLoading: this.setLoading,
      user: null,
      setUser: this.setUser,
      userImages: [],
      setUserImages: this.setUserImages,
      userLabels: [],
      setUserLabels: this.setUserLabels,

      // AutoML Datas
      datasetName: null,
      setDatasetName: this.setDatasetName,
      operationId: null,
      setOperationId: this.setOperationId,
      modelName: null,
      setModelName: this.setModelName,

      // Token OAuth
      tokenAutoml: null,
      setTokenAutoml: this.setTokenAutoml,
      tokenDialogflow: null,
      setTokenDialogflow: this.setTokenDialogflow,
    };
  }

  setLanguage = (language) => {
    this.setState({ language });
  };

  setArEnable = (arEnable) => {
    this.setState({ arEnable });
  };

  setArSupported = (arSupported) => {
    this.setState({ arSupported });
  };

  setAllPermissions = (allPermissions) => {
    this.setState({ allPermissions });
  };

  setLoading = (loading) => {
    this.setState({ loading });
  };

  setUser = (user) => {
    this.setState({ user });
  };

  setUserImages = (userImages) => {
    this.setState({ userImages });
  };

  setUserLabels = (userLabels) => {
    this.setState({ userLabels });
  };

  setDatasetName = (datasetName) => {
    this.setState({ datasetName });
  };

  setOperationId = (operationId) => {
    this.setState({ operationId });
  };

  setModelName = (modelName) => {
    this.setState({ modelName });
  };


  setTokenAutoml = (tokenAutoml) => {
    this.setState({ tokenAutoml });
  };

  setTokenDialogflow = (tokenDialogflow) => {
    this.setState({ tokenDialogflow });
  };

  render() {
    const { children } = this.props;
    return (
      <AppContext.Provider value={this.state}>
        {children}
      </AppContext.Provider>
    );
  }
}

AppProvider.propTypes = {
  children: PropTypes.shape().isRequired,
};

export const AppConsumer = AppContext.Consumer;

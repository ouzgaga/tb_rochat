/**
 * Classe pour les différentes couleurs des rectangles pour l'entourage des objets détectés
 */
const rectanglesColor = [
  {
    name: 'rectanglesColor0',
    color: '#e6194b',
  },
  {
    name: 'rectanglesColor1',
    color: '#ffe119',
  },
  {
    name: 'rectanglesColor2',
    color: '#3cb44b',
  },
  {
    name: 'rectanglesColor3',
    color: '#46f0f0',
  },
  {
    name: 'rectanglesColor4',
    color: '#4363d8',
  },
  {
    name: 'rectanglesColor5',
    color: '#FF44EF',
  },
  {
    name: 'rectanglesColor6',
    color: '#4CFF00',
  },
  {
    name: 'rectanglesColor7',
    color: '#B200FF',
  },
  {
    name: 'rectanglesColor8',
    color: '#FF6A00',
  },
  {
    name: 'rectanglesColor9',
    color: '#3700FF',
  },
];

export default rectanglesColor;

/**
 * Permet d'appeler cloud vision API et de traiter les résultats
 */
import GoogleTranslation from '../api/GoogleTranslation';
import GoogleVision from '../api/GoogleVision';
import Multilangue from './Multilingue';

/**
 * Appelle un service de google selon le mode fournit
 * @param {*} mode le mode à appeler
 * @param {*} image l'image à analyser
 */
const fetchGoogleVisionAndTranslateMode = async (mode, image) => {
  if (mode === 0) {
    return fetchGoogleVisionObjectLocalizationAPIAndTranslate(image, 5, Multilangue.getCurrentLocale());
  }
  if (mode === 1) {
    return fetchGoogleVisionObjectLabelDetectionAPIAndTranslate(image, 5, Multilangue.getCurrentLocale());
  }

  return [];
};

/**
 * Permet d'appeler l'API de localisation et de traduire les résultats
 * @param {*} image l'image est à analyser
 * @param {*} maxResults le nombre de résultat maximum
 * @param {*} langage le langage final
 */
const fetchGoogleVisionObjectLocalizationAPIAndTranslate = async (image, maxResults, langage) => {
  const objectsDetected = [];
  const promises = [];

  const response = await GoogleVision.fetchGoogleVisionAPI(
    image,
    'OBJECT_LOCALIZATION',
    maxResults,
  );

  if (
    response && (Object.keys(response.responses[0]).length !== 0 || response.responses[0].constructor !== Object)) {
    response.responses[0].localizedObjectAnnotations.forEach(async (detectedObject) => {
      if (detectedObject.score >= 0.6) {
        promises.push(new Promise((async (resolve) => {
          let objectName;
          if (langage !== 'en') {
            objectName = await GoogleTranslation.translationGoogle(detectedObject.name, 'en', langage);
          } else {
            objectName = detectedObject.name;
          }

          const vertices = detectedObject.boundingPoly.normalizedVertices;

          // on récupère les bords sur l'écran des objets détectés
          // Google ne renvoie pas les x ou les y s'ils sont au bord de l'image
          const x1 = vertices[0].x ? vertices[0].x : 0;
          const x2 = vertices[1].x ? vertices[1].x : 1;
          const y1 = vertices[0].y ? vertices[0].y : 0;
          const y2 = vertices[2].y ? vertices[2].y : 1;

          const nbObjectName = objectsDetected.filter(item => item.name.includes(objectName)).length;

          if (nbObjectName) {
            objectName += ' ';
            objectName += nbObjectName.toString();
          }

          const newObject = {
            name: objectName,
            vertices: {
              x1, x2, y1, y2,
            },
            score: (detectedObject.score * 100).toFixed(0),
          };

          objectsDetected.push(newObject);
          resolve();
        })));
      }
    });
  }

  return Promise.all(promises).then(() => objectsDetected.sort((a, b) => b.score - a.score));
};

/**
 * Permet d'appeler l'API de classification et de traduire les résultats
 * @param {*} image l'image est à analyser
 * @param {*} maxResults le nombre de résultat maximum
 * @param {*} langage le langage final
 */
const fetchGoogleVisionObjectLabelDetectionAPIAndTranslate = async (image, maxResults, langage) => {
  const labelsDetected = [];
  const promises = [];

  const response = await GoogleVision.fetchGoogleVisionAPI(
    image,
    'LABEL_DETECTION',
    maxResults,
  );

  if (
    response && (Object.keys(response.responses[0]).length !== 0 || response.responses[0].constructor !== Object)) {
    response.responses[0].labelAnnotations.forEach(async (label) => {
      if (label.score >= 0.6) {
        promises.push(new Promise((async (resolve) => {
          let objectName;
          if (langage !== 'en') {
            objectName = await GoogleTranslation.translationGoogle(label.description, 'en', langage);
          } else {
            objectName = label.description;
          }

          const newLabel = {
            name: objectName,
            score: (label.score * 100).toFixed(0),
          };

          labelsDetected.push(newLabel);
          resolve();
        })));
      }
    });
  }

  return Promise.all(promises).then(() => labelsDetected.sort((a, b) => b.score - a.score));
};

export default { fetchGoogleVisionAndTranslateMode };

import Voice from 'react-native-voice';

let onVoiceResults2;
let onSpeechError2;

const onSpeechResults = (e) => {
  onVoiceResults2(e.value);
};

const onSpeechError = (e) => {
  onSpeechError2(e);
};

const startRecognizing = async (language, onVoiceResults, onVoiceError) => {
  onVoiceResults2 = onVoiceResults;
  onSpeechError2 = onVoiceError;
  Voice.onSpeechResults = onSpeechResults;
  Voice.onSpeechError = onSpeechError;
  try {
    await Voice.start(language);
  } catch (e) {
    console.error(e);
  }
};

export default { startRecognizing };

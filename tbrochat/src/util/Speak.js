import Tts from 'react-native-tts';

// Fichier permettant de transcrire du texte en audio (Text-to-Speech)
const speak = (text, language) => {
  Tts.setDefaultLanguage(language);
  Tts.speak(text);
};

export default { speak };

/**
 * Check les différentes permissions nécessaires
 */
import {
  Platform,
} from 'react-native';
import Permissions from 'react-native-permissions';

const checkPermissions = async () => {
  // Android et iOS permissions
  if (await Permissions.request('camera') !== 'authorized') {
    return false;
  }
  if (await Permissions.request('photo') !== 'authorized') {
    return false;
  }
  if (await Permissions.request('microphone') !== 'authorized') {
    return false;
  }

  // iOS permissions
  if (Platform.OS === 'ios') {
    if (await Permissions.request('speechRecognition') !== 'authorized') {
      return false;
    }
    if (await Permissions.request('mediaLibrary') !== 'authorized') {
      return false;
    }

    // Android permissions
  } else if (Platform.OS === 'android') {
    if (await Permissions.request('storage') !== 'authorized') {
      return false;
    }
  }

  return true;
};

export default checkPermissions;

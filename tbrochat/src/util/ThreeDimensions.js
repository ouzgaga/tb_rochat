/**
 * Classe qui contient des fonctions utiles pour des calculs en 3 dimensions
 */

/**
 * Retourne la distance entre deux points
 * @param {*} PointA le premier point
 * @param {*} PointB le deuxième point
 */
const distanceBetweenTwoPoints = (PointA, PointB) => Math.sqrt(((PointB[0] - PointA[0]) * (PointB[0] - PointA[0])) + ((PointB[1] - PointA[1]) * (PointB[1] - PointA[1])) + ((PointB[2] - PointA[2]) * (PointB[2] - PointA[2])));

/**
 * Retourne la norme d'un vecteur
 * @param {*} vector le vecteur
 */
const normeVector = vector => Math.sqrt(vector[0] * vector[0] + vector[1] * vector[1] + vector[2] * vector[2]);

/**
 * Retourne le produit scalaire de deux vecteur
 * @param {*} vectorA le 1er vecteur
 * @param {*} vectorB le 2eme vecteur
 */
const scalarProduct = (vectorA, vectorB) => vectorA[0] * vectorB[0] + vectorA[1] * vectorB[1] + vectorA[2] * vectorB[2];

/**
 * Projection horizontale d'un vecteur
 * @param {*} vector le vcteur à projeter
 */
const horizontalPlaneProjection = vector => [vector[0], 0, vector[2]];

/**
 * Calcule l'angle entre 2 vecteurs
 * @param {*} vectorA le 1er vecteur
 * @param {*} vectorB le 2eme vecteur
 */
const angleBetweenTwoVectors = (vectorA, vectorB) => Math.acos(scalarProduct(vectorA, vectorB) / (normeVector(vectorA) * normeVector(vectorB)));

/**
 * Calcule l'angle entre deux vecteur dans le plan horizontal
 * @param {*} vectorA le 1er vecteur
 * @param {*} vectorB le 2eme vecteur
 */
const angleBetweenTwoVectorsInHorizontalPlaneProjection = (vectorA, vectorB) => (angle(vectorA[0], vectorA[2]) - angle(vectorB[0], vectorB[2])) * 180 / Math.PI;

const angle = (x, y) => Math.atan(y / x);

const determinantHorizontal = (vectorA, vectorB) => vectorA[0] * vectorB[2] - vectorA[2] * vectorB[0];

const determinantVertical = (vectorA, vectorB) => vectorA[1] * vectorB[2] - vectorA[2] * vectorB[1];

export default {
  distanceBetweenTwoPoints,
  normeVector,
  scalarProduct,
  horizontalPlaneProjection,
  angleBetweenTwoVectors,
  angleBetweenTwoVectorsInHorizontalPlaneProjection,
  angle,
  determinantHorizontal,
  determinantVertical,
};

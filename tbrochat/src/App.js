import React, { useEffect, useContext } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import 'firebase/firestore';

import * as firebase from 'firebase';
import { StyleProvider, Root } from 'native-base';
import Tts from 'react-native-tts';

import getTheme from './assets/native-base-theme/components';
import material from './assets/native-base-theme/variables/material';
import { AppContext, AppProvider } from './util/Store';

import FirebaseConfig from './constants/Firebase';
import AppNavigator from './navigation/AppNavigator';
import Loader from './components/Loader';
import './util/TimerFix';
import Firebase from './api/Firebase';

function App() {
  const appContext = useContext(AppContext);
  const {
    setUser,
    setUserLabels,
    setUserImages,
    setDatasetName,
    setOperationId,
    setModelName,
    loading,
  } = appContext;

  useEffect(() => {
    firebase.initializeApp(FirebaseConfig);
    checkIfLoggedIn();
  }, []);

  const checkIfLoggedIn = () => {
    firebase.auth().onAuthStateChanged((user) => {
      setUser(user);
      // si l'utilisateur est connecté
      if (user) {
        // on observe les modifications des labels
        firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('labels')
          .onSnapshot(async () => {
            setUserLabels(await Firebase.getLabels());
          });

        // on observe les modifications des images
        firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('images')
          .onSnapshot(async () => {
            setUserImages(await Firebase.getImages());
          });

        // on observe les modifications des datasets et modèles pour AutoML
        firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid)
          .onSnapshot(async () => {
            const autoMLDatas = await Firebase.getUserAutoMLDatas();

            if (autoMLDatas) {
              const { datasetName, operationId, modelName } = autoMLDatas;

              setDatasetName(datasetName);
              setOperationId(operationId);
              setModelName(modelName);
            }
          });
      } else {
        setUserLabels([]);
        setUserImages([]);
        setDatasetName(null);
        setOperationId(null);
        setModelName(null);
      }
    });
  };

  return (
    <StyleProvider style={getTheme(material)}>
      <Root>
        <View style={styles.container}>
          <Loader visible={loading} />
          <AppNavigator />
        </View>
      </Root>
    </StyleProvider>
  );
}


export default () => (
  <AppProvider>
    <App />
  </AppProvider>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

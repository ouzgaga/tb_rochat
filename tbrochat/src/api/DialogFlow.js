/**
 * Permet de détecter des Intent sur notre modèle Dialogflow
 * @param {*} bearer le token OAuth 2.0 pour l'authentificatoin
 * @param {*} textToQuery La requête de l'utilisateur
 * @param {*} sessionId L'id de la session de l'utilisatuer
 * @param {*} language La langue utilisée par l'utilisateur
 */
const detectIntent = async (bearer, textToQuery, sessionId, language) => {
  try {
    const url = `https://dialogflow.googleapis.com/v2beta1/projects/tbrochat/agent/sessions/${sessionId}:detectIntent`;

    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${bearer}`,
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        queryInput: {
          text: {
            text: textToQuery,
            languageCode: language,
          },
        },
      }),
    });
    const responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export default {
  detectIntent,
};

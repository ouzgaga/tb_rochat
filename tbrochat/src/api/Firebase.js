/**
 * Permet d'utiliser les différentes fonctionnalités de firebase
 */
import * as firebase from 'firebase';
import 'firebase/firestore';
import uuid from 'uuid';

/**
 * Retourne l'uid de l'utilisateur
 */
const getUid = () => firebase.auth().currentUser.uid;

/**
 * Retourne les labels que l'utilisateur a créé
 */
const getLabels = async () => {
  const labels = await firebase.firestore()
    .collection('datasets')
    .doc(firebase.auth().currentUser.uid)
    .collection('labels')
    .get();

  const returnLabels = [];
  labels.forEach((label) => { returnLabels.push({ labelId: label.id, data: label.data() }); });

  return returnLabels.sort((a, b) => ((a.data.name > b.data.name) ? 1 : -1));
};

/**
 * Retourne le lien vers les images de l'utilisateur ainsi que leurs label associé
 */
const getImages = async () => {
  const images = await firebase.firestore()
    .collection('datasets')
    .doc(firebase.auth().currentUser.uid)
    .collection('images')
    .get();

  const returnImages = [];
  images.forEach((image) => { returnImages.push({ imageId: image.id, data: image.data() }); });

  return returnImages;
};

/**
 * Permet d'obtenir les données AutoML d'un utilisateur (comme l'id de son modèle)
 */
const getUserAutoMLDatas = async () => {
  const doc = await firebase.firestore()
    .collection('datasets')
    .doc(firebase.auth().currentUser.uid)
    .get();

  if (doc.exists) {
    return doc.data();
  }

  return null;
};

/**
 * Permet de créer un nouveau label pour l'utilisateur
 */
const createLabel = (label) => {
  const labelsRef = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('labels')
    .doc();

  labelsRef.set({ name: label });
};

/**
 * Permet de changer le nom du label
 */
const changeLabelName = (labelId, newLabelName) => {
  const labelsRef = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('labels')
    .doc(labelId);
  labelsRef.update({ name: newLabelName });
};

/**
 * Permet de supprimer le label
 */
const deleteLabel = (labelId) => {
  const labelsRef = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('labels')
    .doc(labelId);
  labelsRef.delete();
};

/**
 * Permet d'ajouter une image avec son label dans firebase
 */
const addImage = (fullPath, downloadLink, label) => {
  const imageRef = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('images')
    .doc();

  imageRef.set({ fullPath, downloadLink, label });
};

/**
 * Permet d'uploader une image dans Firebase Storage
 * @param {*} blob L'image à stocker
 */
const addImageToStorage = async (blob) => {
  const ImageRef = firebase
    .storage()
    .ref(`/${firebase.auth().currentUser.uid}/`)
    .child(`${uuid.v4()}.jpg`);
  const { ref } = await ImageRef.put(blob);

  return { fullPath: `gs://${ref.bucket}/${ref.fullPath}`, downloadLink: await ref.getDownloadURL() };
};

/**
 * Permet d'ajouter le fichier CSV contenant les images avec leurs labels dans Firebase Storage
 */
const addCSVToStorage = async (blob) => {
  const CSVRef = firebase
    .storage()
    .ref(`/${firebase.auth().currentUser.uid}/`)
    .child(`${uuid.v4()}.csv`);
  const { ref } = await CSVRef.put(blob);

  return `gs://${ref.bucket}/${ref.fullPath}`;
};

/**
 * Permet de changer le label attribué à une image
 * @param {*} imageId L'id de l'image dont le label doit changer
 * @param {*} newLabel Le nouveau label attribuéà l'image
 */
const changeLabelForImage = (imageId, newLabel) => {
  const imageRef = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('images')
    .doc(imageId);

  imageRef.update({
    label: newLabel,
  });
};

/**
 * Permet de supprimer une image de firestore
 * @param {*} imageId l'id de l'image à supprimer
 */
const deleteImage = (imageId) => {
  firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid).collection('images')
    .doc(imageId)
    .delete()
    .then(() => {
    })
    .catch((error) => {
      console.error('Error removing document: ', error);
    });
};

/**
 * Permet d'ajouter le dataset d'images labellisées
 * @param {*} datasetName2 l'id du dataset
 */
const setAutoMLDataset = (datasetName2) => {
  const ref = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid);
  ref.set({ datasetName: datasetName2 }, { merge: true });
};

/**
 * Permet d'ajouter le model à firestore
 * @param {*} modelName2 l'id du model
 */
const setAutoMLModel = (modelName2) => {
  const ref = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid);
  ref.set({ modelName: modelName2 }, { merge: true });
};

/**
 * Permet d'ajouter l'opération en cours à firestore
 * @param {*} operationId2 l'id de l'opération en cours
 */
const setAutoMLOperation = (operationId2) => {
  const ref = firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid);
  ref.set({ operationId: operationId2 }, { merge: true });
};

/**
 * Permet de récuperer les données pour la personnalisation d'un utilisateur
 */
const getAutoMLDatas = async () => {
  const docRef = await firebase.firestore().collection('datasets').doc(firebase.auth().currentUser.uid);

  try {
    const doc = await docRef.get();
    if (doc.exists) {
      return doc.data();
    }
  } catch (err) {
    console.log(err);
  }
  return null;
};

/**
 * Permet de se déconnecter de firebase
 */
const signOut = async () => {
  await firebase.auth().signOut();
};

export default {
  getUid,
  getLabels,
  getImages,
  getUserAutoMLDatas,
  createLabel,
  changeLabelName,
  deleteLabel,
  addImage,
  changeLabelForImage,
  deleteImage,
  addImageToStorage,
  addCSVToStorage,
  setAutoMLDataset,
  setAutoMLModel,
  setAutoMLOperation,
  getAutoMLDatas,
  signOut,
};

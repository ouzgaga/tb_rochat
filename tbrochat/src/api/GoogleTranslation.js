/**
 * Permet d'utiliser l'API Translation de Google
 */
import APIKeys from '../constants/APIKeys';

/**
 * Permet d'obtenir l'url de base
 * @param {*} textToTranslate le texte à traduire
 * @param {*} sourceLanguage le langage de base
 * @param {*} targetLanguage le langage d'arrivée
 */
const getQueryURL = (textToTranslate, sourceLanguage, targetLanguage) => {
  const baseURL = 'https://translation.googleapis.com/language/translate/v2?';

  let url = `${baseURL}key=${APIKeys.API_GOOGLE}`;
  url += `&q=${textToTranslate}`;
  url += `&source=${sourceLanguage}`;
  url += `&target=${targetLanguage}`;
  url += '&format=text';

  return url;
};

/**
 * Permet de faire une traduction d'un texte
 * @param {*} textToTranslate le texte à traduire
 * @param {*} sourceLanguage le langage de base
 * @param {*} targetLanguage le langage d'arrivée
 */
const translationGoogle = async (textToTranlate, sourceLanguage, targetLanguage) => {
  try {
    const url = getQueryURL(textToTranlate, sourceLanguage, targetLanguage);
    const response = await fetch(url);
    const responseJson = await response.json();
    return responseJson.data.translations[0].translatedText;
  } catch (error) {
    console.log(error);
    return null; // TODO
  }
};

export default { translationGoogle };

/**
 * Permet d'utiliser l'API Google Cloud Vision
 */
import APIKeys from '../constants/APIKeys';

/**
 * Permet d'obtenir l'url de base
 */
const getQueryURL = () => {
  const baseURL = 'https://vision.googleapis.com/v1/images:annotate?';
  const url = `${baseURL}key=${APIKeys.API_GOOGLE}`;
  return url;
};

/**
 * Permet d'obtenir le body de la requête POST
 * @param {*} imageBase64 l'image en base 64
 * @param {*} featureType le mode que l'on veut utiliser
 * @param {*} maxResults le nombre de résultats maximum que l'on veut
 */
const getBody = (imageBase64, featureType, maxResults) => JSON.stringify({
  requests: [
    {
      features: [{ type: featureType, maxResults }],
      image: {
        content: imageBase64,
      },
    },
  ],
});

/**
 * Permet d'analyser une image
 * @param {*} imageBase64 l'image en base 64
 * @param {*} featureType le mode que l'on veut utiliser
 * @param {*} maxResults le nombre de résultats maximum que l'on veut
 */
const fetchGoogleVisionAPI = async (imageBase64, featureType, maxResults) => {
  try {
    const body = getBody(imageBase64, featureType, maxResults);
    const url = getQueryURL();

    const response = await fetch(url, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body,
    });
    const responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};


export default { fetchGoogleVisionAPI };

/**
 * Permet d'utiliser les fonctionnalités de Google Cloud AutoML
 */

/**
 * URL de base
 */
const getQueryURL = () => {
  const baseURL = 'https://automl.googleapis.com/v1beta1/projects/tbrochat/locations/us-central1/datasets';
  return baseURL;
};

/**
 * Permet de créer un nouveau dataset
 * @param {*} bearer le token OAuth 2.0
 * @param {*} userUid l'id de l'utilisateur
 */
const createDataset = async (bearer, userUid) => {
  try {
    const url = getQueryURL();

    const body = JSON.stringify({
      displayName: userUid,
      imageClassificationDatasetMetadata: {
        classificationType: 'MULTICLASS',
      },
    });

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'POST',
      body,
    });
    const responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet de récupérer tous les datasets
 * @param {*} bearer le token OAuth 2.0
 */
const getAllDatasets = async (bearer) => {
  try {
    const url = getQueryURL();

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'GET',
    });
    const responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet d'importer les images labelisées qui sont dans un fichier CSV
 * @param {*} bearer le token OAuth 2.0
 * @param {*} csvUri Le lien vers le fichier CSV
 * @param {*} datasetName le nom du dataser
 */
const importCSVToDataset = async (bearer, csvUri, datasetName) => {
  try {
    const url = `https://automl.googleapis.com/v1beta1/${datasetName}:importData`;

    const body = JSON.stringify({
      inputConfig: {
        gcsSource: {
          inputUris: [
            csvUri,
          ],
        },
      },
    });

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'POST',
      body,
    });
    const responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet d'obtenir les détails sur une opération en cours
 * @param {*} bearer le token OAuth 2.0
 * @param {*} operationName le nom de l'operation
 */
const getOperationDetails = async (bearer, operationName) => {
  try {
    const url = `https://automl.googleapis.com/v1beta1/${operationName}`;

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'GET',
    });
    const responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet d'annuler une opération en cours
 * @param {*} bearer le token OAuth 2.0
 * @param {*} operationName le nom de l'opération à annuler
 */
const cancelOperation = async (bearer, operationName) => {
  try {
    const url = `https://automl.googleapis.com/v1beta1/${operationName}:cancel`;

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'POST',
    });
    const responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet d'entrainer un modèle après avoir importer le fichier CSV contenant les images labellisées
 * @param {*} bearer le token OAuth 2.0
 * @param {*} datasetId l'id du dataset à entrainer
 * @param {*} userUid l'uid de l'utilisateur
 */
const trainModel = async (bearer, datasetId, userUid) => {
  const str = datasetId.split('/');
  const id = str[str.length - 1];
  try {
    const url = 'https://automl.googleapis.com/v1beta1/projects/tbrochat/locations/us-central1/models';

    const body = JSON.stringify({
      displayName: userUid,
      dataset_id: id,
      imageClassificationModelMetadata: {
        trainBudget: '1',
      },
    });

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'POST',
      body,
    });
    const responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet de supprimer un dataset et donc le modèle qui y est attaché (s'il y en a un)
 * @param {*} bearer le token OAuth 2.0
 * @param {*} datasetId l'id du dataset
 */
const deleteDataset = async (bearer, datasetId) => {
  try {
    const url = `https://automl.googleapis.com/v1beta1/${datasetId}`;

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'DELETE',
    });
    const responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet d'obtenir les statistiques sur le modèle
 * @param {*} bearer le token OAuth 2.0
 * @param {*} modelName le nom du modèle dont on veut les statistiques
 */
const getModelStatistics = async (bearer, modelName) => {
  try {
    const url = `https://automl.googleapis.com/v1beta1/${modelName}/modelEvaluations`;

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'GET',
    });
    const responseJson = await response.json();
    return responseJson;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet d'obtenir le nom d'un label à partir de son id
 * @param {*} bearer le token OAuth 2.0
 * @param {*} datasetName le nom du dataset qui contient le label
 * @param {*} annotationSpecId l'id de l'annotation correspondant au label
 */
const getLabelDisplayName = async (bearer, datasetName, annotationSpecId) => {
  try {
    const url = `https://automl.googleapis.com/v1beta1/${datasetName}/annotationSpecs/${annotationSpecId}`;

    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${bearer}`,
      },
      method: 'GET',
    });
    const responseJson = await response.json();
    return responseJson.displayName;
  } catch (error) {
    console.log(error);
    return null;
  }
};

/**
 * Permet de faire une prédiction sur le modèle
 * @param {*} bearer le token OAuth 2.0
 * @param {*} modelName le nom du modèle
 * @param {*} image l'image en base 64 à prédire
 */
const imagePrediction = async (bearer, modelName, image) => {
  try {
    const url = `https://automl.googleapis.com/v1beta1/${modelName}:predict`;

    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${bearer}`,
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify({
        payload: {
          image: {
            imageBytes: image,
          },
        },
      }),
    });
    const responseJson = await response.json();
    const resultArray = [];
    responseJson.payload.forEach((item) => {
      resultArray.push({ name: item.displayName, score: (item.classification.score * 100).toFixed(0) });
    });

    return resultArray.sort((a, b) => b.score - a.score);
  } catch (error) {
    console.log(error);
    return null;
  }
};

export default {
  createDataset, getAllDatasets, importCSVToDataset, getOperationDetails, trainModel, deleteDataset, cancelOperation, getModelStatistics, getLabelDisplayName, imagePrediction,
};

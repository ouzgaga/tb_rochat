module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'max-len': 'off', // enlève longueur ligne maximum
    'react/jsx-filename-extension': [1, { "extensions": [".js", ".jsx"] }], // permet extension jsx
    'no-use-before-define': 'off',
    'no-loop-func': 'off',
    'react/no-unused-state': 'off',
    'react/jsx-indent': 'off',
    'indent': 'off',
    'react/no-multi-comp': 'off',
    'no-nested-ternary': 'off',
    'no-await-in-loop': 'off',
    'camelcase': 'off',
  },
};

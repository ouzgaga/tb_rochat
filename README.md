# TB_Rochat

### tbrochat

#### Instructions de déploiement Android

Installer les dépendances du projet

```
yarn install
```

Lancer l'application en mode debug

```
react-native run-android
```



#### Lancer l'application en mode release

Le fichier \android\app\build\outputs\bundle\release\app.aab est un Android App Bundle généré et prêt à être uploader sur Google Play

Pour tester le build release de l'application, vous pouvez effectuer la commande suivante

```
react-native run-android --variant=release
```



#### Instructions de déploiement iOS

*Note : Dans l'état actuel, l'application ne permet malheureusement pas de faire tourner l'application sur iOS*. 

### Documentation

Contient tous les documents relatifs à la documentation tels que

- les articles lus
- les rapports
- les images analysées avec Google Cloud Vision et Amazon Rekognition

